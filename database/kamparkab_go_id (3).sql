-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 17 Mar 2023 pada 09.36
-- Versi server: 5.7.33
-- Versi PHP: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kamparkab.go.id`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_dinas`
--

CREATE TABLE `tb_dinas` (
  `kode_dinas` varchar(15) NOT NULL,
  `nama_dinas` varchar(45) NOT NULL,
  `keterangan` varchar(128) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_dinas`
--

INSERT INTO `tb_dinas` (`kode_dinas`, `nama_dinas`, `keterangan`, `created_at`) VALUES
('0', 'No Dinas', 'No Dinas', '2023-02-28 03:10:33'),
('11', 'Dinas Perikanan', 'Dinas Perikanan', '2023-02-28 03:10:33'),
('12', 'Dinas Pertanian', 'Dinas Pertanian', '2023-02-28 03:10:33'),
('13', 'Dinas Peternakan', 'Dinas Peternakan', '2023-02-28 03:10:33'),
('14', 'Dinas Perkebunan', 'Dinas Perkebunan', '2023-02-28 03:10:33'),
('15', 'Dinas Ketahanan Pangan', 'Dinas Ketahanan Pangan', '2023-02-28 03:10:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kecamatan`
--

CREATE TABLE `tb_kecamatan` (
  `id_kecamatan` int(11) NOT NULL,
  `kd_kecamatan` varchar(25) DEFAULT NULL,
  `nama_kecamatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kecamatan`
--

INSERT INTO `tb_kecamatan` (`id_kecamatan`, `kd_kecamatan`, `nama_kecamatan`) VALUES
(1, '1406010', 'Kampar Kiri'),
(2, '1406011', 'Kampar Kiri Hulu'),
(3, '1406012', 'Kampar Kiri Hilir'),
(4, '1406013', 'Gunung Sahilan'),
(5, '1406014', 'Kampar Kiri Tengah'),
(6, '1406020', 'Xiii Koto Kampar'),
(7, '1406021', 'Koto Kampar Hulu'),
(8, '1406030', 'Kuok'),
(9, '1406031', 'Salo'),
(10, '1406040', 'Tapung'),
(11, '1406041', 'Tapung Hulu'),
(12, '1406042', 'Tapung Hilir'),
(13, '1406050', 'Bangkinang Kota'),
(14, '1406051', 'Bangkinang'),
(15, '1406060', 'Kampar'),
(16, '1406061', 'Kampa'),
(17, '1406062', 'Rumbio Jaya'),
(18, '1406063', 'Kampar Utara'),
(19, '1406070', 'Tambang'),
(20, '1406080', 'Siak Hulu'),
(21, '1406081', 'Perhentian Raja'),
(22, '0', 'No Opsi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_menu`
--

CREATE TABLE `tb_menu` (
  `id_menu` varchar(10) NOT NULL,
  `urutan` int(11) NOT NULL,
  `nama_menu` varchar(50) NOT NULL,
  `url` varchar(128) DEFAULT NULL,
  `icon` varchar(128) DEFAULT NULL,
  `rec_stat` char(1) NOT NULL,
  `keterangan` varchar(45) NOT NULL,
  `tgl_created` datetime NOT NULL,
  `tgl_updated` datetime NOT NULL,
  `user_created` varchar(20) NOT NULL,
  `user_updated` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_menu`
--

INSERT INTO `tb_menu` (`id_menu`, `urutan`, `nama_menu`, `url`, `icon`, `rec_stat`, `keterangan`, `tgl_created`, `tgl_updated`, `user_created`, `user_updated`) VALUES
('M1', 1, 'Dashboard', 'dashboard', 'fas fa-chart-pie', 'A', '', '2020-07-17 04:00:05', '2021-07-20 23:36:32', 'administrator', 'administrator'),
('M2', 2, 'Setting Pengguna', '', 'fas fa-users-cog', 'A', 'Menu Module Setting Sistem', '2021-01-27 03:01:47', '2021-07-19 18:03:24', 'administrator', 'administrator'),
('M3', 3, 'Setting Role', '', 'fas fa-user-cog', 'A', 'Menu Module Setting Sistem', '2021-03-26 14:06:47', '2022-05-16 19:46:04', 'administrator', 'administrator'),
('M4', 4, 'Setting Module', '', 'fas fa-question-circle', 'A', 'Menu Module Setting Sistem', '2020-07-17 03:58:24', '2022-07-19 01:01:40', 'administrator', 'administrator'),
('M5', 5, 'Setting Menu', '', 'far fa-credit-card', 'A', 'Menu Module Setting Sistem', '2021-07-22 17:15:17', '2022-05-16 19:44:54', 'administrator', 'administrator'),
('M6', 6, 'Data Master', 'master', 'far fa-credit-card', 'A', 'Menu Module Dinas Perkebunan', '2021-07-22 17:15:17', '2022-05-16 19:44:54', 'administrator', 'administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_modules`
--

CREATE TABLE `tb_modules` (
  `id_modules` varchar(15) NOT NULL,
  `nama_modules` varchar(25) NOT NULL,
  `is_active` varchar(2) NOT NULL,
  `keterangan` varchar(25) NOT NULL,
  `url_home` varchar(25) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_modules`
--

INSERT INTO `tb_modules` (`id_modules`, `nama_modules`, `is_active`, `keterangan`, `url_home`, `created_at`) VALUES
('MDL1', 'dns_perikanan', 'Y', 'Dinas Perikanan', 'dns_perikanan/home', '2023-02-28 03:37:42'),
('MDL2', 'dns_perkebunan', 'Y', 'Dinas Perkebunan', 'dns_perkebunan/home', '2023-02-28 03:37:42'),
('MDL3', 'setting_sistem', 'Y', 'Setting Sistem', NULL, '2023-02-28 03:37:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_modules_menu`
--

CREATE TABLE `tb_modules_menu` (
  `id_modules_menu` int(15) NOT NULL,
  `id_modules` varchar(15) NOT NULL,
  `id_menu` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_modules_menu`
--

INSERT INTO `tb_modules_menu` (`id_modules_menu`, `id_modules`, `id_menu`) VALUES
(1, 'MDL3', 'M2'),
(2, 'MDL3', 'M3'),
(3, 'MDL3', 'M4'),
(4, 'MDL1', 'M6'),
(5, 'MDL2', 'M6');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_modules_role`
--

CREATE TABLE `tb_modules_role` (
  `id_menu_role` int(11) NOT NULL,
  `id_modules` varchar(10) NOT NULL,
  `id_role` varchar(10) NOT NULL,
  `tgl_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tgl_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_created` varchar(20) DEFAULT NULL,
  `user_updated` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_modules_role`
--

INSERT INTO `tb_modules_role` (`id_menu_role`, `id_modules`, `id_role`, `tgl_created`, `tgl_updated`, `user_created`, `user_updated`) VALUES
(3, 'MDL3', 'R1', '2023-03-02 06:45:38', '2023-03-02 06:45:38', NULL, NULL),
(6, 'MDL2', 'R2', '2023-03-02 06:45:38', '2023-03-02 06:45:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_role`
--

CREATE TABLE `tb_role` (
  `id_role` varchar(10) NOT NULL,
  `nama_role` varchar(50) NOT NULL,
  `rec_stat` char(1) NOT NULL,
  `tgl_created` datetime NOT NULL,
  `tgl_updated` datetime NOT NULL,
  `user_created` varchar(20) NOT NULL,
  `user_updated` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_role`
--

INSERT INTO `tb_role` (`id_role`, `nama_role`, `rec_stat`, `tgl_created`, `tgl_updated`, `user_created`, `user_updated`) VALUES
('R1', 'Superadmin', 'A', '2020-07-17 02:52:08', '2021-01-28 10:42:18', 'administrator', 'administrator'),
('R2', 'Admin Dinas', 'A', '2020-07-17 02:52:08', '2022-05-16 16:09:10', 'administrator', 'administrator'),
('R3', 'Operator', 'A', '2021-01-20 04:08:37', '2022-05-16 16:12:07', 'administrator', 'administrator'),
('R4', 'Penyuluh', 'A', '2022-05-16 10:19:38', '2022-05-16 10:19:38', 'administrator', 'administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_submenu`
--

CREATE TABLE `tb_submenu` (
  `id_submenu` varchar(10) NOT NULL,
  `urutan` int(11) NOT NULL,
  `nama_submenu` varchar(50) NOT NULL,
  `url` varchar(128) DEFAULT NULL,
  `icon` varchar(128) DEFAULT NULL,
  `rec_stat` char(1) NOT NULL,
  `tgl_created` datetime NOT NULL,
  `tgl_updated` datetime NOT NULL,
  `user_created` varchar(20) NOT NULL,
  `user_updated` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_submenu`
--

INSERT INTO `tb_submenu` (`id_submenu`, `urutan`, `nama_submenu`, `url`, `icon`, `rec_stat`, `tgl_created`, `tgl_updated`, `user_created`, `user_updated`) VALUES
('SM1', 1, 'Pengguna', 'user', 'fas fa-user', 'A', '2022-04-07 00:11:29', '2022-04-07 00:11:29', 'administrator', 'administrator'),
('SM2', 2, 'Pengguna Role', 'user', 'fas fa-user', 'A', '2022-04-07 00:11:29', '2022-04-07 00:11:29', 'administrator', 'administrator'),
('SM3', 3, 'Module', 'user', 'fas fa-user', 'A', '2022-04-07 00:11:29', '2022-04-07 00:11:29', 'administrator', 'administrator'),
('SM4', 4, 'Module Menu', 'user', 'fas fa-user', 'A', '2022-04-07 00:11:29', '2022-04-07 00:11:29', 'administrator', 'administrator'),
('SM5', 5, 'Role', 'user', 'fas fa-user', 'A', '2022-04-07 00:11:29', '2022-04-07 00:11:29', 'administrator', 'administrator'),
('SM6', 6, 'Role Hak Akses', 'user', 'fas fa-user', 'A', '2022-04-07 00:11:29', '2022-04-07 00:11:29', 'administrator', 'administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_submenu_menu`
--

CREATE TABLE `tb_submenu_menu` (
  `id_submenu_menu` int(11) NOT NULL,
  `id_submenu` varchar(10) NOT NULL,
  `id_menu` varchar(10) NOT NULL,
  `tgl_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tgl_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_created` varchar(20) DEFAULT NULL,
  `user_updated` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_submenu_menu`
--

INSERT INTO `tb_submenu_menu` (`id_submenu_menu`, `id_submenu`, `id_menu`, `tgl_created`, `tgl_updated`, `user_created`, `user_updated`) VALUES
(1, 'SM1', 'M2', '2023-03-02 05:48:04', '2023-03-02 05:48:04', NULL, NULL),
(2, 'SM2', 'M2', '2023-03-02 05:48:04', '2023-03-02 05:48:04', NULL, NULL),
(3, 'SM3', 'M3', '2023-03-02 05:48:04', '2023-03-02 05:48:04', NULL, NULL),
(4, 'SM4', 'M3', '2023-03-02 05:48:04', '2023-03-02 05:48:04', NULL, NULL),
(5, 'SM5', 'M4', '2023-03-02 05:48:04', '2023-03-02 05:48:04', NULL, NULL),
(6, 'SM6', 'M4', '2023-03-02 05:48:04', '2023-03-02 05:48:04', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `nip` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telepon` varchar(20) NOT NULL,
  `is_user_login` char(1) DEFAULT NULL,
  `tgl_created` datetime NOT NULL,
  `tgl_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`nip`, `password`, `nama`, `email`, `telepon`, `is_user_login`, `tgl_created`, `tgl_updated`) VALUES
('123456', '60a9f4cb52d2d87b7052c7aa69b98e36', 'Super Admin', 'superadmin@mail.com', '09999999', 'Y', '2023-02-27 20:24:19', '2023-02-27 20:24:19'),
('disbun', '60a9f4cb52d2d87b7052c7aa69b98e36', 'Admin Dinas Perkebunan', 'disbun@mail.com', '09999999', 'Y', '2023-02-27 20:24:19', '2023-02-27 20:24:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user_role`
--

CREATE TABLE `tb_user_role` (
  `id_user_role` int(11) NOT NULL,
  `kode_dinas` varchar(5) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `id_role` varchar(10) NOT NULL,
  `tgl_created` datetime NOT NULL,
  `tgl_updated` datetime NOT NULL,
  `user_created` varchar(20) NOT NULL,
  `user_updated` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_user_role`
--

INSERT INTO `tb_user_role` (`id_user_role`, `kode_dinas`, `nip`, `id_role`, `tgl_created`, `tgl_updated`, `user_created`, `user_updated`) VALUES
(1, '0', '123456', 'R1', '2023-02-27 20:27:40', '2023-02-27 20:27:40', 'superadmin', 'superadmin'),
(2, '14', 'disbun', 'R2', '2023-02-27 20:27:40', '2023-02-27 20:27:40', 'superadmin', 'superadmin');

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_menu`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_menu` (
`id_modules` varchar(15)
,`id_menu` varchar(10)
,`nama_menu` varchar(50)
,`url` varchar(128)
,`rec_stat` char(1)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_submenu_menu`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_submenu_menu` (
`id_submenu_menu` int(11)
,`id_menu` varchar(10)
,`nama_menu` varchar(50)
,`rec_stat` char(1)
,`id_submenu` varchar(10)
,`nama_submenu` varchar(50)
,`urutan` int(11)
,`url` varchar(128)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `v_menu`
--
DROP TABLE IF EXISTS `v_menu`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_menu`  AS SELECT `a`.`id_modules` AS `id_modules`, `b`.`id_menu` AS `id_menu`, `b`.`nama_menu` AS `nama_menu`, `b`.`url` AS `url`, `b`.`rec_stat` AS `rec_stat` FROM (`tb_modules_menu` `a` join `tb_menu` `b` on((`a`.`id_menu` = `b`.`id_menu`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_submenu_menu`
--
DROP TABLE IF EXISTS `v_submenu_menu`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_submenu_menu`  AS SELECT `a`.`id_submenu_menu` AS `id_submenu_menu`, `a`.`id_menu` AS `id_menu`, `c`.`nama_menu` AS `nama_menu`, `c`.`rec_stat` AS `rec_stat`, `b`.`id_submenu` AS `id_submenu`, `b`.`nama_submenu` AS `nama_submenu`, `b`.`urutan` AS `urutan`, `b`.`url` AS `url` FROM ((`tb_submenu_menu` `a` join `tb_submenu` `b` on((`a`.`id_submenu` = `b`.`id_submenu`))) join `tb_menu` `c` on((`a`.`id_menu` = `c`.`id_menu`))) ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_dinas`
--
ALTER TABLE `tb_dinas`
  ADD PRIMARY KEY (`kode_dinas`);

--
-- Indeks untuk tabel `tb_kecamatan`
--
ALTER TABLE `tb_kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indeks untuk tabel `tb_menu`
--
ALTER TABLE `tb_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indeks untuk tabel `tb_modules`
--
ALTER TABLE `tb_modules`
  ADD PRIMARY KEY (`id_modules`);

--
-- Indeks untuk tabel `tb_modules_menu`
--
ALTER TABLE `tb_modules_menu`
  ADD PRIMARY KEY (`id_modules_menu`),
  ADD KEY `id_modules` (`id_modules`),
  ADD KEY `id_menu` (`id_menu`);

--
-- Indeks untuk tabel `tb_modules_role`
--
ALTER TABLE `tb_modules_role`
  ADD PRIMARY KEY (`id_menu_role`),
  ADD KEY `FK_IDROLE_MR_idx` (`id_role`),
  ADD KEY `id_modules_menu` (`id_modules`);

--
-- Indeks untuk tabel `tb_role`
--
ALTER TABLE `tb_role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indeks untuk tabel `tb_submenu`
--
ALTER TABLE `tb_submenu`
  ADD PRIMARY KEY (`id_submenu`);

--
-- Indeks untuk tabel `tb_submenu_menu`
--
ALTER TABLE `tb_submenu_menu`
  ADD PRIMARY KEY (`id_submenu_menu`),
  ADD KEY `FK_IDMENU_SM_idx` (`id_menu`),
  ADD KEY `FK_IDSUBMENU_SM_idx` (`id_submenu`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`nip`);

--
-- Indeks untuk tabel `tb_user_role`
--
ALTER TABLE `tb_user_role`
  ADD PRIMARY KEY (`id_user_role`),
  ADD KEY `FK_IDROLE_UR_idx` (`id_role`),
  ADD KEY `FK_NIPP_UR_idx` (`nip`),
  ADD KEY `INDEX_CABANG_UR` (`kode_dinas`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_kecamatan`
--
ALTER TABLE `tb_kecamatan`
  MODIFY `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `tb_modules_menu`
--
ALTER TABLE `tb_modules_menu`
  MODIFY `id_modules_menu` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tb_modules_role`
--
ALTER TABLE `tb_modules_role`
  MODIFY `id_menu_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_submenu_menu`
--
ALTER TABLE `tb_submenu_menu`
  MODIFY `id_submenu_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_user_role`
--
ALTER TABLE `tb_user_role`
  MODIFY `id_user_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_modules_menu`
--
ALTER TABLE `tb_modules_menu`
  ADD CONSTRAINT `tb_modules_menu_ibfk_1` FOREIGN KEY (`id_modules`) REFERENCES `tb_modules` (`id_modules`),
  ADD CONSTRAINT `tb_modules_menu_ibfk_2` FOREIGN KEY (`id_menu`) REFERENCES `tb_menu` (`id_menu`);

--
-- Ketidakleluasaan untuk tabel `tb_modules_role`
--
ALTER TABLE `tb_modules_role`
  ADD CONSTRAINT `tb_modules_role_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `tb_role` (`id_role`),
  ADD CONSTRAINT `tb_modules_role_ibfk_2` FOREIGN KEY (`id_modules`) REFERENCES `tb_modules` (`id_modules`);

--
-- Ketidakleluasaan untuk tabel `tb_submenu_menu`
--
ALTER TABLE `tb_submenu_menu`
  ADD CONSTRAINT `tb_submenu_menu_ibfk_1` FOREIGN KEY (`id_submenu`) REFERENCES `tb_submenu` (`id_submenu`),
  ADD CONSTRAINT `tb_submenu_menu_ibfk_2` FOREIGN KEY (`id_menu`) REFERENCES `tb_menu` (`id_menu`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
