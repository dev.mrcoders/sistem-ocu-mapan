-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 21 Mar 2023 pada 08.35
-- Versi server: 5.7.33
-- Versi PHP: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kamparkab.go.id_disnak`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_komoditi`
--

CREATE TABLE `tb_komoditi` (
  `id_komoditi` int(5) NOT NULL,
  `nama_komoditi` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_komoditi`
--

INSERT INTO `tb_komoditi` (`id_komoditi`, `nama_komoditi`) VALUES
(1, 'Sapi potong Lokal'),
(2, 'Sapi potong Eks Impor (BX)'),
(3, 'Sapi Perah'),
(4, 'Kerbau Potong'),
(5, 'Kerbau Perah'),
(6, 'Kambing Potong'),
(7, 'Kambing Perah'),
(8, 'Domba'),
(9, 'Babi'),
(10, 'Kuda'),
(11, 'Ayam Buras'),
(12, 'Ayam Ras Petelur'),
(13, 'Ayam ras Pedaging'),
(14, 'Kelinci'),
(15, 'Merpati'),
(16, 'Burung Puyuh'),
(17, 'Itik'),
(18, 'Itik Manila');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_komoditi`
--
ALTER TABLE `tb_komoditi`
  ADD PRIMARY KEY (`id_komoditi`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_komoditi`
--
ALTER TABLE `tb_komoditi`
  MODIFY `id_komoditi` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
