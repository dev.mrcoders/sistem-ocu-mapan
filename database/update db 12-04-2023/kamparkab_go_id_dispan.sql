-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 12, 2023 at 07:32 AM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kamparkab.go.id_dispan`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_komoditi`
--

CREATE TABLE `tb_komoditi` (
  `id_komoditi` int(5) NOT NULL,
  `nama_komoditi` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_komoditi`
--

INSERT INTO `tb_komoditi` (`id_komoditi`, `nama_komoditi`) VALUES
(1, 'Padi Sawah');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_komoditi`
--
ALTER TABLE `tb_komoditi`
  ADD PRIMARY KEY (`id_komoditi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_komoditi`
--
ALTER TABLE `tb_komoditi`
  MODIFY `id_komoditi` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
