-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 12, 2023 at 07:32 AM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kamparkab.go.id_disnak`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_komoditi`
--

CREATE TABLE `tb_komoditi` (
  `id_komoditi` int(5) NOT NULL,
  `nama_komoditi` varchar(45) NOT NULL,
  `daging` set('Y','N') NOT NULL,
  `telur` set('Y','N') NOT NULL,
  `susu` set('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_komoditi`
--

INSERT INTO `tb_komoditi` (`id_komoditi`, `nama_komoditi`, `daging`, `telur`, `susu`) VALUES
(1, 'Sapi potong Lokal', 'Y', 'N', 'N'),
(2, 'Sapi potong Eks Impor (BX)', 'Y', 'N', 'N'),
(3, 'Sapi Perah', 'N', 'N', 'Y'),
(4, 'Kerbau Potong', 'Y', 'N', 'N'),
(5, 'Kerbau Perah', 'N', 'N', 'Y'),
(6, 'Kambing Potong', 'Y', 'N', 'N'),
(7, 'Kambing Perah', 'N', 'N', 'Y'),
(8, 'Domba', 'Y', 'N', 'N'),
(9, 'Babi', 'Y', 'N', 'N'),
(10, 'Kuda', 'Y', 'N', 'N'),
(11, 'Ayam Buras', 'Y', 'N', 'N'),
(12, 'Ayam Ras Petelur', 'N', 'Y', 'N'),
(13, 'Ayam ras Pedaging', 'Y', 'Y', 'N'),
(14, 'Kelinci', 'Y', 'N', 'N'),
(15, 'Merpati', 'Y', 'N', 'N'),
(16, 'Burung Puyuh', 'N', 'Y', 'N'),
(17, 'Itik', 'Y', 'N', 'N'),
(18, 'Itik Manila', 'Y', 'N', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_master`
--

CREATE TABLE `tb_transaksi_master` (
  `id_transaksi_master` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `bulan` varchar(11) NOT NULL,
  `tahun` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_transaksi_master`
--

INSERT INTO `tb_transaksi_master` (`id_transaksi_master`, `id_kecamatan`, `bulan`, `tahun`) VALUES
(1, 1, '1', '2022'),
(2, 1, '2', '2022');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_populasi_lkl`
--

CREATE TABLE `tb_transaksi_populasi_lkl` (
  `id_transaksi_populasi` int(11) NOT NULL,
  `id_transaksi_master` int(11) NOT NULL,
  `id_komoditi` int(11) NOT NULL,
  `kategori` enum('P-A','P-M','P-D') NOT NULL,
  `lahir` int(11) NOT NULL,
  `mati` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_transaksi_populasi_lkl`
--

INSERT INTO `tb_transaksi_populasi_lkl` (`id_transaksi_populasi`, `id_transaksi_master`, `id_komoditi`, `kategori`, `lahir`, `mati`, `jumlah`, `total`) VALUES
(1, 1, 1, 'P-A', 1, 3, 10, 8),
(2, 1, 1, 'P-M', 0, 3, 4, 1),
(3, 1, 1, 'P-D', 0, 2, 10, 8);

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_produksi`
--

CREATE TABLE `tb_transaksi_produksi` (
  `id_transaksi_produksi` int(11) NOT NULL,
  `id_transaksi_master` int(11) NOT NULL,
  `id_komoditi` int(11) NOT NULL,
  `kakas` varchar(25) NOT NULL,
  `pemotongan` varchar(25) NOT NULL,
  `produksi` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_transaksi_produksi`
--

INSERT INTO `tb_transaksi_produksi` (`id_transaksi_produksi`, `id_transaksi_master`, `id_komoditi`, `kakas`, `pemotongan`, `produksi`) VALUES
(1, 1, 1, '127.61', '32', '4084');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_trx`
--

CREATE TABLE `tb_transaksi_trx` (
  `id_transaksi_populasi` int(11) NOT NULL,
  `id_transaksi_master` int(11) NOT NULL,
  `id_komoditi` int(11) NOT NULL,
  `populasi` int(11) NOT NULL,
  `populasi_sp` int(11) NOT NULL,
  `pemasukan` int(11) NOT NULL,
  `pengeluaran` int(11) NOT NULL,
  `kelahiran` int(11) NOT NULL,
  `kematian` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_transaksi_trx`
--

INSERT INTO `tb_transaksi_trx` (`id_transaksi_populasi`, `id_transaksi_master`, `id_komoditi`, `populasi`, `populasi_sp`, `pemasukan`, `pengeluaran`, `kelahiran`, `kematian`) VALUES
(1, 1, 1, 0, 13, 10, 5, 1, 8);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_komoditi`
--
ALTER TABLE `tb_komoditi`
  ADD PRIMARY KEY (`id_komoditi`);

--
-- Indexes for table `tb_transaksi_master`
--
ALTER TABLE `tb_transaksi_master`
  ADD PRIMARY KEY (`id_transaksi_master`);

--
-- Indexes for table `tb_transaksi_populasi_lkl`
--
ALTER TABLE `tb_transaksi_populasi_lkl`
  ADD PRIMARY KEY (`id_transaksi_populasi`);

--
-- Indexes for table `tb_transaksi_produksi`
--
ALTER TABLE `tb_transaksi_produksi`
  ADD PRIMARY KEY (`id_transaksi_produksi`);

--
-- Indexes for table `tb_transaksi_trx`
--
ALTER TABLE `tb_transaksi_trx`
  ADD PRIMARY KEY (`id_transaksi_populasi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_komoditi`
--
ALTER TABLE `tb_komoditi`
  MODIFY `id_komoditi` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tb_transaksi_master`
--
ALTER TABLE `tb_transaksi_master`
  MODIFY `id_transaksi_master` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_transaksi_populasi_lkl`
--
ALTER TABLE `tb_transaksi_populasi_lkl`
  MODIFY `id_transaksi_populasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_transaksi_produksi`
--
ALTER TABLE `tb_transaksi_produksi`
  MODIFY `id_transaksi_produksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_transaksi_trx`
--
ALTER TABLE `tb_transaksi_trx`
  MODIFY `id_transaksi_populasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
