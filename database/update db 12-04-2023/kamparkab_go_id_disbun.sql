-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 12, 2023 at 07:32 AM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kamparkab.go.id_disbun`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_kepemilikan`
--

CREATE TABLE `tb_kepemilikan` (
  `id_kepemilikan` int(15) NOT NULL,
  `kepemilikan` varchar(25) NOT NULL,
  `keterangan` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_kepemilikan`
--

INSERT INTO `tb_kepemilikan` (`id_kepemilikan`, `kepemilikan`, `keterangan`) VALUES
(1, 'PBS', 'Swastaa'),
(3, 'PMN', 'Negara'),
(4, 'MR', 'Rakyat');

-- --------------------------------------------------------

--
-- Table structure for table `tb_komoditi`
--

CREATE TABLE `tb_komoditi` (
  `id_komoditi` int(5) NOT NULL,
  `nama_komoditi` varchar(45) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_komoditi`
--

INSERT INTO `tb_komoditi` (`id_komoditi`, `nama_komoditi`) VALUES
(1, 'Karet'),
(2, 'Kelapa Sawit'),
(3, 'Kelapa'),
(4, 'Gambir'),
(5, 'Kakao'),
(6, 'Kopi'),
(7, 'Kemiri'),
(8, 'Pinang'),
(9, 'Kapuk'),
(20, 'Lada'),
(22, 'Cengkeh');

-- --------------------------------------------------------

--
-- Table structure for table `tb_periode`
--

CREATE TABLE `tb_periode` (
  `id_periode` int(15) NOT NULL,
  `semester` int(5) NOT NULL,
  `tahun` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_periode`
--

INSERT INTO `tb_periode` (`id_periode`, `semester`, `tahun`) VALUES
(1, 1, 2023),
(2, 2, 2023),
(3, 1, 2022),
(4, 2, 2022),
(5, 1, 2024);

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_edit`
--

CREATE TABLE `tb_transaksi_edit` (
  `id_transaksi_edit` int(11) NOT NULL,
  `id_transaksi_master` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_komoditi` int(11) NOT NULL,
  `id_kepemilikan` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `tbm_lalu` int(11) NOT NULL,
  `tbm_ini_to_tm` int(11) NOT NULL,
  `tbm_ini_to_ttr` int(11) NOT NULL,
  `tm_lalu` int(11) NOT NULL,
  `tm_ini_to_ttr` int(11) NOT NULL,
  `ttr_lalu` int(11) NOT NULL,
  `ttr_ini_to_tbm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_transaksi_edit`
--

INSERT INTO `tb_transaksi_edit` (`id_transaksi_edit`, `id_transaksi_master`, `id_kecamatan`, `id_komoditi`, `id_kepemilikan`, `id_periode`, `tbm_lalu`, `tbm_ini_to_tm`, `tbm_ini_to_ttr`, `tm_lalu`, `tm_ini_to_ttr`, `ttr_lalu`, `ttr_ini_to_tbm`) VALUES
(1, 1, 1, 1, 4, 1, 55, 0, 0, 367, 0, 57, 0),
(2, 2, 1, 2, 1, 1, 12, 0, 0, 45, 2, 12, 12);

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_lahan`
--

CREATE TABLE `tb_transaksi_lahan` (
  `id_lahan` int(15) NOT NULL,
  `id_transaksi_master` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_komoditi` int(11) NOT NULL,
  `id_kepemilikan` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `luas_lahan` varchar(15) NOT NULL DEFAULT '0',
  `total` varchar(15) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_transaksi_lahan`
--

INSERT INTO `tb_transaksi_lahan` (`id_lahan`, `id_transaksi_master`, `id_kecamatan`, `id_komoditi`, `id_kepemilikan`, `id_periode`, `luas_lahan`, `total`) VALUES
(1, 1, 1, 1, 4, 1, '', '479'),
(2, 2, 1, 2, 1, 1, '12', '81');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_master`
--

CREATE TABLE `tb_transaksi_master` (
  `id_transaksi_master` int(15) NOT NULL,
  `id_periode` int(15) NOT NULL,
  `id_kepemilikan` int(15) NOT NULL,
  `id_komoditi` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `user_created` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_transaksi_master`
--

INSERT INTO `tb_transaksi_master` (`id_transaksi_master`, `id_periode`, `id_kepemilikan`, `id_komoditi`, `created_at`, `user_created`) VALUES
(1, 1, 4, 1, '2023-03-16 19:21:20', 'admin'),
(2, 1, 1, 2, '2023-03-28 10:26:09', 'admin'),
(3, 1, 1, 1, '2023-03-28 10:29:43', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_petani`
--

CREATE TABLE `tb_transaksi_petani` (
  `id_transaksi_petani` int(11) NOT NULL,
  `id_transaksi_master` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_komoditi` int(11) NOT NULL,
  `id_kepemilikan` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `jumlah_kk` int(15) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_transaksi_petani`
--

INSERT INTO `tb_transaksi_petani` (`id_transaksi_petani`, `id_transaksi_master`, `id_kecamatan`, `id_komoditi`, `id_kepemilikan`, `id_periode`, `jumlah_kk`) VALUES
(1, 1, 1, 1, 4, 1, 160),
(2, 2, 1, 2, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_produksi`
--

CREATE TABLE `tb_transaksi_produksi` (
  `id_transaksi_produksi` int(15) NOT NULL,
  `id_transaksi_master` int(15) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_komoditi` int(11) NOT NULL,
  `id_kepemilikan` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `produksi` varchar(15) NOT NULL,
  `rata_rata` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_transaksi_produksi`
--

INSERT INTO `tb_transaksi_produksi` (`id_transaksi_produksi`, `id_transaksi_master`, `id_kecamatan`, `id_komoditi`, `id_kepemilikan`, `id_periode`, `produksi`, `rata_rata`) VALUES
(1, 1, 1, 1, 4, 1, '715', '1986'),
(2, 2, 1, 2, 1, 1, '12', '222');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_rekap`
--

CREATE TABLE `tb_transaksi_rekap` (
  `id_transaksi_rekap` int(15) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_komoditi` int(11) NOT NULL,
  `id_kepemilikan` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `total_luas_lahan` varchar(15) NOT NULL,
  `total_produksi` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_transaksi_rekap`
--

INSERT INTO `tb_transaksi_rekap` (`id_transaksi_rekap`, `id_kecamatan`, `id_komoditi`, `id_kepemilikan`, `id_periode`, `total_luas_lahan`, `total_produksi`) VALUES
(1, 1, 1, 4, 1, '479', '715'),
(2, 1, 2, 1, 1, '81', '12');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_tanaman`
--

CREATE TABLE `tb_transaksi_tanaman` (
  `id_transaksi_tanaman` int(15) NOT NULL,
  `id_transaksi_master` int(11) NOT NULL,
  `id_kecamatan` int(15) NOT NULL,
  `id_komoditi` int(11) NOT NULL,
  `id_kepemilikan` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `tbm` varchar(15) NOT NULL,
  `tm` varchar(15) NOT NULL,
  `ttr` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_transaksi_tanaman`
--

INSERT INTO `tb_transaksi_tanaman` (`id_transaksi_tanaman`, `id_transaksi_master`, `id_kecamatan`, `id_komoditi`, `id_kepemilikan`, `id_periode`, `tbm`, `tm`, `ttr`) VALUES
(1, 1, 1, 1, 4, 1, '53', '360', '66'),
(2, 2, 1, 2, 1, 1, '36', '43', '2');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_rekap_data`
-- (See below for the actual view)
--
CREATE TABLE `v_rekap_data` (
`id_transaksi_rekap` int(15)
,`id_kecamatan` int(11)
,`id_komoditi` int(11)
,`id_kepemilikan` int(11)
,`id_periode` int(11)
,`nama_komoditi` varchar(45)
,`kepemilikan` varchar(25)
,`keterangan` varchar(45)
,`semester` int(5)
,`tahun` year(4)
,`total_luas_lahan` varchar(15)
,`total_produksi` varchar(15)
);

-- --------------------------------------------------------

--
-- Structure for view `v_rekap_data`
--
DROP TABLE IF EXISTS `v_rekap_data`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_rekap_data`  AS SELECT `a`.`id_transaksi_rekap` AS `id_transaksi_rekap`, `a`.`id_kecamatan` AS `id_kecamatan`, `a`.`id_komoditi` AS `id_komoditi`, `a`.`id_kepemilikan` AS `id_kepemilikan`, `a`.`id_periode` AS `id_periode`, `b`.`nama_komoditi` AS `nama_komoditi`, `c`.`kepemilikan` AS `kepemilikan`, `c`.`keterangan` AS `keterangan`, `d`.`semester` AS `semester`, `d`.`tahun` AS `tahun`, `a`.`total_luas_lahan` AS `total_luas_lahan`, `a`.`total_produksi` AS `total_produksi` FROM (((`tb_transaksi_rekap` `a` join `tb_komoditi` `b` on((`a`.`id_komoditi` = `b`.`id_komoditi`))) join `tb_kepemilikan` `c` on((`a`.`id_kepemilikan` = `c`.`id_kepemilikan`))) join `tb_periode` `d` on((`a`.`id_periode` = `d`.`id_periode`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_kepemilikan`
--
ALTER TABLE `tb_kepemilikan`
  ADD PRIMARY KEY (`id_kepemilikan`);

--
-- Indexes for table `tb_komoditi`
--
ALTER TABLE `tb_komoditi`
  ADD PRIMARY KEY (`id_komoditi`);

--
-- Indexes for table `tb_periode`
--
ALTER TABLE `tb_periode`
  ADD PRIMARY KEY (`id_periode`);

--
-- Indexes for table `tb_transaksi_edit`
--
ALTER TABLE `tb_transaksi_edit`
  ADD PRIMARY KEY (`id_transaksi_edit`);

--
-- Indexes for table `tb_transaksi_lahan`
--
ALTER TABLE `tb_transaksi_lahan`
  ADD PRIMARY KEY (`id_lahan`);

--
-- Indexes for table `tb_transaksi_master`
--
ALTER TABLE `tb_transaksi_master`
  ADD PRIMARY KEY (`id_transaksi_master`);

--
-- Indexes for table `tb_transaksi_petani`
--
ALTER TABLE `tb_transaksi_petani`
  ADD PRIMARY KEY (`id_transaksi_petani`);

--
-- Indexes for table `tb_transaksi_produksi`
--
ALTER TABLE `tb_transaksi_produksi`
  ADD PRIMARY KEY (`id_transaksi_produksi`);

--
-- Indexes for table `tb_transaksi_rekap`
--
ALTER TABLE `tb_transaksi_rekap`
  ADD PRIMARY KEY (`id_transaksi_rekap`);

--
-- Indexes for table `tb_transaksi_tanaman`
--
ALTER TABLE `tb_transaksi_tanaman`
  ADD PRIMARY KEY (`id_transaksi_tanaman`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_kepemilikan`
--
ALTER TABLE `tb_kepemilikan`
  MODIFY `id_kepemilikan` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_komoditi`
--
ALTER TABLE `tb_komoditi`
  MODIFY `id_komoditi` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tb_periode`
--
ALTER TABLE `tb_periode`
  MODIFY `id_periode` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_transaksi_edit`
--
ALTER TABLE `tb_transaksi_edit`
  MODIFY `id_transaksi_edit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_transaksi_lahan`
--
ALTER TABLE `tb_transaksi_lahan`
  MODIFY `id_lahan` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_transaksi_master`
--
ALTER TABLE `tb_transaksi_master`
  MODIFY `id_transaksi_master` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_transaksi_petani`
--
ALTER TABLE `tb_transaksi_petani`
  MODIFY `id_transaksi_petani` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_transaksi_produksi`
--
ALTER TABLE `tb_transaksi_produksi`
  MODIFY `id_transaksi_produksi` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_transaksi_rekap`
--
ALTER TABLE `tb_transaksi_rekap`
  MODIFY `id_transaksi_rekap` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_transaksi_tanaman`
--
ALTER TABLE `tb_transaksi_tanaman`
  MODIFY `id_transaksi_tanaman` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
