<?php
/**
 * 
 */
class Access
{
	private $ci;
	private $nip;
	private $table=[
		'user_role'=>'tb_user_role',
		'role'=>'tb_role',
		'modules'=>'tb_modules',
		'modules_role'=>'tb_modules_role'
	];

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->nip = $this->ci->session->userdata('nip');
	}

	public function GetRole($nip)
	{
		$this->ci->db->where('nip', $nip);
		$get_role = $this->ci->db->get($this->table['user_role'])->row();

		return $get_role;	
	}

	function GetKodeDinas(){
		$this->ci->db->select('kode_dinas');
		$this->ci->db->where('nip', $this->nip);
		$get_dinas = $this->ci->db->get($this->table['user_role'])->row();

		return $get_dinas;
	}

	function GetModules($nip = null){
		if ($nip != null) {
			$this->nip = $nip;
		}
		
		$kode_dinas = $this->GetKodeDinas()->kode_dinas;
		$role_user = $this->GetRole($this->nip)->id_role;
		$this->ci->db->select('b.id_modules,b.nama_modules');
		$this->ci->db->join($this->table['modules'].' b', 'a.id_modules=b.id_modules', 'inner');
		$this->ci->db->where('a.kode_dinas',$kode_dinas);
		$this->ci->db->where('a.id_role', $role_user);
		$get_dinas = $this->ci->db->get($this->table['modules_role'].' a')->row();

		return $get_dinas;
	}

	function BulanIndo($bln){
		$bulan = array (
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);

		return $bulan[ $bln ] ;
	}

	public function GetMenuPerkebunan()
	{
		$list_menus = [
			[
				'nama_menu'=>'Dashboard',
				'url'=>'home',
				'icon'=>'mdi-home',
				'submenu'=>null
			],
			[
				'nama_menu'=>'Data Master',
				'url'=>'#',
				'icon'=>'mdi-view-list',
				'submenu'=>[
					[
						'nama_menu'=>'Komoditi',
						'url'=>'komoditi'
					],
					[
						'nama_menu'=>'Periode',
						'url'=>'periode'
					],
					[
						'nama_menu'=>'Kepemilikan',
						'url'=>'kepemilikan'
					]
				]
			],
			[
				'nama_menu'=>'Data Transaksi',
				'url'=>'transaksi',
				'icon'=>'mdi-apps',
				'submenu'=>null
			],
			[
				'nama_menu'=>'Rekap Data',
				'url'=>'rekap',
				'icon'=>'mdi-apps',
				'submenu'=>null
			],
			
		];

		$obj = json_decode(json_encode($list_menus));
		return $obj;
	}

	public function GetMenuPerternakan()
	{
		$list_menus = [
			[
				'nama_menu'=>'Dashboard',
				'url'=>'home',
				'icon'=>'mdi-home',
				'submenu'=>null
			],
			[
				'nama_menu'=>'Data Komoditi',
				'url'=>'komoditi',
				'icon'=>'mdi-apps',
				'submenu'=>null
			],
			[
				'nama_menu'=>'Data Transaksi',
				'url'=>'transaksi',
				'icon'=>'mdi-apps',
				'submenu'=>null
			],
			[
				'nama_menu'=>'Rekap Populasi',
				'url'=>'rekap',
				'icon'=>'mdi-apps',
				'submenu'=>null
			],
			
		];

		$obj = json_decode(json_encode($list_menus));
		return $obj;
	}

	public function GetMenuPertanian()
	{
		$list_menus = [
			[
				'nama_menu'=>'Dashboard',
				'url'=>'home',
				'icon'=>'mdi-home',
				'submenu'=>null
			],
			[
				'nama_menu'=>'Data Komoditi',
				'url'=>'komoditi',
				'icon'=>'mdi-apps',
				'submenu'=>null
			],
			[
				'nama_menu'=>'Data Transaksi',
				'url'=>'transaksi',
				'icon'=>'mdi-apps',
				'submenu'=>null
			],
			
		];

		$obj = json_decode(json_encode($list_menus));
		return $obj;
	}
}

?>
