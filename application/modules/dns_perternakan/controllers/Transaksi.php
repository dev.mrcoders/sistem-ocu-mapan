<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends MY_Controller {

	private $data=[];
	private $nip;
	var $csrf_name = '';
	var $csrf_hash;
	var $json = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_transaksi');
		$this->load->model('M_komoditi');
		$this->load->model('master/M_kecamatan','kecamatan');
		$this->csrf_name = 'csrf_param';
		$this->csrf_hash = $this->security->get_csrf_hash();
	}

	public function index()
	{
		$this->data['title'] = 'Ini Home';
		$this->view('transaksi.index', $this->data);	
	}

	public function viewData($sid)
	{ 
		$data_master = $this->M_transaksi->GetById($sid);
		$this->data['title'] = 'Ini Home';
		$this->data['sid'] = $sid;
		$this->data['detail'] = $data_master;
		$this->data['kecamatan'] = $this->dataKecamatan($data_master->id_kecamatan);
		$this->view('transaksi.view', $this->data);	
	}

	public function DataTables()
	{
		$list = $this->M_transaksi->get_datatables();
		$data = array();
		$no = $this->input->post('start');
		//looping data mahasiswa
		foreach ($list as $r) {
			$no++;
			$row = array();

			$row['no'] = $no;
			$row['sid'] = $r->id_transaksi_master;
			$row['kecamatan'] = $this->dataKecamatan($r->id_kecamatan);
			$row['bulan'] = $r->bulan;
			$row['tahun'] = $r->tahun;

			$data[] = $row;
		}
		$this->json = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->M_transaksi->count_all(),
			"recordsFiltered" => $this->M_transaksi->count_filtered(),
			"data" => $data,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
		
	}

	public function dataKecamatan($id)
	{
		$data_kec = $this->kecamatan->GetKecamatanById($id);

		return $data_kec->nama_kecamatan;
	}


	public function DataTablesView()
	{
		$sid_trx = 1;
		$list = $this->M_komoditi->get_datatables();
		$data = array();
		$no = $this->input->post('start');
		foreach ($list as $r) {
			$no++;
			$row = array();

			$row['no'] = $no;
			$row['nama'] = $r->nama_komoditi;
			$row['id_komoditi'] = $r->id_komoditi;
			$row['data_trx'] = $this->dataTransaksi($sid_trx,$r->id_komoditi);
			$row['data_plokal'] = $this->dataPlokal($sid_trx,$r->id_komoditi);
			$row['data_prod'] = $this->dataProduksi($sid_trx,$r->id_komoditi);

			$data[] = $row;
		}
		$this->json = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->M_komoditi->count_all(),
			"recordsFiltered" => $this->M_komoditi->count_filtered(),
			"data" => $data,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function DataById()
	{
		$data_trx = $this->M_transaksi->GetById();

		$this->json = array(
			"success" => true,
			"message" => "DATA FOUND",
			"data" => $data_trx,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function DetailById()
	{
		$sid = $this->input->get('sid');
		$idkm = $this->input->get('idkm');

		$data_trx =[
			'data_trx'=>$this->dataTransaksi($sid,$idkm)['data'],
			'data_plokal' => $this->dataPlokal($sid,$idkm)['data'],
			'data_prod' => $this->dataProduksi($sid,$idkm)['data'],
			'data_kom' => $this->M_komoditi->DetailKom($idkm),
		];

		$this->json = array(
			"success" => true,
			"message" => "DATA FOUND",
			"data" => $data_trx,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function dataTransaksi($sid,$idkdm)
	{
		$where = ['id_transaksi_master'=>$sid,'id_komoditi'=>$idkdm ];
		$data_trx = $this->M_transaksi->GetTrxData('tb_transaksi_trx',$where);

		$this->json = array(
			"success" => false,
			"message" => "DATA NOT FOUND",
			"data" => 0,
		);
		if ($data_trx) {
			$this->json = array(
				"success" => true,
				"message" => "DATA FOUND",
				"data" => $data_trx,
			);
		}

		return $this->json;
	}

	public function dataProduksi($sid,$idkdm)
	{
		$where = ['id_transaksi_master'=>$sid,'id_komoditi'=>$idkdm ];
		$data_trx = $this->M_transaksi->GetTrxData('tb_transaksi_produksi',$where);

		$this->json = array(
			"success" => false,
			"message" => "DATA NOT FOUND",
			"data" => 0,
		);
		if ($data_trx) {
			$this->json = array(
				"success" => true,
				"message" => "DATA FOUND",
				"data" => $data_trx,
			);
		}

		return $this->json;
	}

	public function dataPlokal($sid,$idkdm)
	{
		$where = ['id_transaksi_master'=>$sid,'id_komoditi'=>$idkdm ];
		$data_trx = $this->M_transaksi->GetTrxData('tb_transaksi_populasi_lkl',$where);

		$this->json = array(
			"success" => false,
			"message" => "DATA NOT FOUND",
			"data" => 0,
		);
		if ($data_trx) {
			$this->json = array(
				"success" => true,
				"message" => "DATA FOUND",
				"data" => $data_trx,
			);
		}

		return $this->json;
	}

	public function Saved()
	{
		$post = $this->input->post();

		$data_transaksi_trx []=[
			'id_transaksi_master'=>$post['id_trx'],
			'id_komoditi'=>$post['id_komoditi'],
			'populasi'=>0,
			'populasi_sp'=>$post['populasi_sp'],
			'pemasukan'=>$post['pemasukan'],
			'pengeluaran'=>$post['pengeluaran'],
			'kematian'=>$post['kematian'],
			'kelahiran'=>$post['kelahiran']
		];

		$data_transaksi_produksi [] = [
			'id_transaksi_master'=>$post['id_trx'],
			'id_komoditi'=>$post['id_komoditi'],
			'kakas'=>$post['kakas'],
			'pemotongan'=>$post['pemotongan'],
			'produksi'=>$post['produksi']['daging'],
			'susu'=>$post['produksi']['susu'],
			'telur'=>$post['produksi']['telur'],
		];

		
		$kategori = ['P-A','P-M','P-D'];

		foreach ($kategori as $key => $value) {
			$data_transaksi_plkl []= [
				'id_transaksi_master'=>$post['id_trx'],
				'id_komoditi'=>$post['id_komoditi'],
				'kategori'=>$value,
				'lahir'=>$post['lahir'][$value],
				'mati'=>$post['mati'][$value],
				'jumlah'=>$post['jumlah'][$value],
				'total'=>$post['total'][$value],
			];
		}

		if($post['mode'] == 'add'){
			$this->M_transaksi->Saved('tb_transaksi_trx',$data_transaksi_trx,true);
			$this->M_transaksi->Saved('tb_transaksi_produksi',$data_transaksi_produksi,true);
			$this->M_transaksi->Saved('tb_transaksi_populasi_lkl',$data_transaksi_plkl,true);

		}else{
			$this->M_transaksi->Updated('tb_transaksi_trx',$data_transaksi_trx,$post['id_trx'],true);
			$this->M_transaksi->Updated('tb_transaksi_produksi',$data_transaksi_produksi,$post['id_trx'],true);
			$this->M_transaksi->Updated('tb_transaksi_populasi_lkl',$data_transaksi_plkl,$post['id_trx'],true);
		}
		$this->json = array(
			"success" => true,
			"message" => "DATA FOUND",
			"data" => [
				'data_transaksi_trx'=>$data_transaksi_trx,
				'data_transaksi_produksi'=>$data_transaksi_produksi,
				'data_transaksi_plkl'=>$data_transaksi_plkl,
			],
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));

	}

	public function CreatedData()
	{
		$data_post = $this->input->post();

		$this->json = array(
			"success" => true,
			"message" => "DATA FOUND",
			"data" => $data_post
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

}

/* End of file Transaksi.php */
/* Location: ./application/modules/dns_perternakan/controllers/Transaksi.php */