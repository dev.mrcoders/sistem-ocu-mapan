<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap extends MY_Controller {
	private $data=[];
	private $nip;
	var $csrf_name = '';
	var $csrf_hash;
	var $json = [];

	public function __construct()
	{
		parent::__construct();
		// $this->load->model('M_rekap');
		$this->load->model('M_komoditi');
		$this->load->model('master/M_kecamatan','kecamatan');
		$this->csrf_name = 'csrf_param';
		$this->csrf_hash = $this->security->get_csrf_hash();
	}

	public function index()
	{
		$this->data['title'] = 'Data Rekap';
		$this->view('rekap.index', $this->data);	
	}

	public function DataTables()
	{
		if($this->input->get('kec')){
			$this->db->where('id_kecamatan', $this->input->get('kec'));
		}
		$list = $this->db->get('tb_kecamatan')->result();
		$data = array();
		$no = $this->input->post('start');
			//looping data mahasiswa
		$kom = $this->M_komoditi->GetById();
		foreach ($list as $r) {
			$no++;
			$row = array();
			foreach ($kom as $key => $v) {
				$row['no']=$no;
				$row['kecamatan'] = $r->nama_kecamatan;
				$row['bulan']= 1;
				$row['ternak']= $v->nama_komoditi;

				$data[] = $row;
			}
			
			

			
		}
		$this->json = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => count($list) + count($kom),
			"recordsFiltered" => count($list) + count($kom),
			"data" => $data,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));

	}


	// public function dataKecamatan($sid)
	// {
	// 	$get_kecamatan = $this->kecamatan->GetKecamatanById($sid);

	// 	return $get_kecamatan->nama_kecamatan;
	// }

}

/* End of file Samples.php */
	/* Location: ./application/modules/Dns_perikanan/controllers/Samples.php */