<!-- Custom CSS -->
<link href="@asset('/')assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
<link href="@asset('/')assets/extra-libs/c3/c3.min.css" rel="stylesheet">
<link href="@asset('/')assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
<!-- Custom CSS -->
<link href="@asset('/')dist/css/style.min.css" rel="stylesheet">
<!-- <link rel="stylesheet" href="@asset('/')assets/fontawesome/css/all.css"> -->
{{-- css datatables --}}

<link href="@asset('assets/')extra-libs/data-tables/datatables.min.css" rel="stylesheet"/>
<link rel="stylesheet" href="@asset('node_modules/')@sweetalert2/theme-bulma/bulma.css">
<link rel="stylesheet" type="text/css" href="@asset('/')assets/libs/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css"
     integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI="
     crossorigin=""/>
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="@asset('/')assets/libs/jquery/dist/jquery.min.js"></script>
<script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js"
     integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM="
     crossorigin=""></script>

<style>
	.w-15 {
		width: 15%!important;
	}
	 .topbar{
		z-index:9993;
	}
	
	.left-sidebar{
		z-index:9992;
	} 
	.modal{
		z-index:9994;
	}
</style>