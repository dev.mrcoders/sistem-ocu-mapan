<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->

        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="in">
                <li class="nav-small-cap">
                    <i class="mdi mdi-dots-horizontal"></i>
                    <span class="hide-menu">Navigation</span>
                </li>
                <?php $menu = $this->CI->access->getmenuperternakan(); ?>
                @foreach($menu as $key=>$menus)
                @if($menus->submenu == null)
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ base_url('dns_perternakan/') }}{{ $menus->url }}" aria-expanded="false">
                        <i class="mdi {{ $menus->icon }}"></i>
                        <span class="hide-menu">{{ $menus->nama_menu }}</span>
                    </a>
                </li>
                @else
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi {{ $menus->icon }}"></i>
                        <span class="hide-menu">{{ $menus->nama_menu }} </span>
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        @foreach($menus->submenu as $submenus)
                        <li class="sidebar-item">
                            <a href="{{ base_url('dns_perternakan/') }}{{ $submenus->url }}" class="sidebar-link">
                                <i class="mdi mdi-adjust"></i>
                                <span class="hide-menu"> {{ $submenus->nama_menu }} </span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </li>
                @endif
                @endforeach
                {{--  --}}
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>