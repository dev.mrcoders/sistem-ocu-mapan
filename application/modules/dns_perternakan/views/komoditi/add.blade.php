<div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah Komoditi Baru</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="form-add-komoditi">
				<div class="modal-body">

					<div class="form-group">
						<label for="recipient-name" class="control-label">Nama Komoditi:</label>
						<input type="text" class="form-control" id="nama_komoditi" name="nama_komoditi" required>
					</div>
					<div class="form-group row">
						<div class="col-sm-12">
							<label for="">Kategori Produksi</label>
						</div>
						<div class="col-sm-4">
							<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="daging" name="daging">
								<label class="custom-control-label" for="daging">Daging</label>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="susu" name="susu">
								<label class="custom-control-label" for="susu">Susu</label>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="telur" name="telur">
								<label class="custom-control-label" for="telur">Telur</label>
							</div>
						</div>
					</div>




				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-danger waves-effect waves-light btn-simpan">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>