@extends('template.index')
@section('content')
<style>
	#map {
		height: 600px;
	}

	.w-15 {
		width: 15% !important;
	}
</style>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<img class="card-img-top" src="https://dkp.kamparkab.go.id/assets/img/logo-pangan.png" alt="Card image cap" style="max-height: 450px">
				<div class="card-img-overlay">

				</div>
				<div class="card-footer bg-dark text-white">
					<div class="row">
						<div class="col-12">
							<div class="row text-center">
								
								<div class="col-6 col-md-3 border-right">
									<div class="m-b-10">KOMODITI</div>
									<img src="@asset('assets/img/svg/list.svg')" alt="" class="w-15">
									<div><span class="font-20">18</span></div>
								</div>
								<div class="col-6 col-md-3 border-right">
									<div class="m-b-10">PRODUKSI DAGING</div>
									<img src="@asset('assets/img/svg/meat.svg')" alt="" class="w-15">
									<div><span class="font-20">1290 kg</span></div>
								</div>
								<div class="col-6 col-md-3 border-right">
									<div class="m-b-10">PRODUKSI TELUR</div>
									<img src="@asset('assets/img/svg/egg.svg')" alt="" class="w-15">
									<div><span class="font-20">12 Ltr</span></div>
								</div>
								<div class="col-6 col-md-3 border-right">
									<div class="m-b-10">PRODUKSI SUSU</div>
									<img src="@asset('assets/img/svg/cow.svg')" alt="" class="w-15">
									<div><span class="font-20">1290 Btr</span></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div id="map"></div>
		</div>
	</div>
</div>

<script>
	$(function() {
		$('#map').removeAttr('tabindex')
		var map = L.map('map').setView([0.22229290451605266, 101.26119967145814], 9);
		L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}', {
			maxZoom: 20,
			subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
		}).addTo(map);

		L.marker([0.34073456126664325, 101.0211539818277]).bindPopup('Kec Bangkinang Kota', {
			autoClose: false
		}).addTo(map).openPopup();
		L.marker([-0.00627597409709324, 101.18345047922486]).bindPopup('Kec Kampar Kiri', {
			autoClose: false
		}).addTo(map).openPopup()
		L.marker([-0.15033344528050918, 101.06558153864603]).bindPopup('Kec Kampar Kiri Hulu', {
			autoClose: false
		}).addTo(map).openPopup();
		L.marker([0.22449031738266, 101.38723948506845]).bindPopup('Kec Kampar Kiri Hilir', {
			autoClose: false
		}).addTo(map).openPopup();

		// var popup1 = new L.Popup({'autoClose':false});
		// popup1.setLatLng([0.34073456126664325, 101.0211539818277]);
		// popup1.setContent('Kec Bangkinang Kota');

		// var popup2 = new L.Popup({'autoClose':false});
		// popup2.setLatLng([-0.00627597409709324, 101.18345047922486]);
		// popup2.setContent('Kec Kampar Kiri');

		// L.marker([0.34073456126664325, 101.0211539818277]).addTo(map)
		// .bindPopup(popup1).openPopup();

		// L.marker([-0.00627597409709324, 101.18345047922486]).addTo(map)
		// .bindPopup(popup2).openPopup();

		setTimeout(function() {
			window.dispatchEvent(new Event("resize"));
		}, 500);
	})
</script>
@endsection