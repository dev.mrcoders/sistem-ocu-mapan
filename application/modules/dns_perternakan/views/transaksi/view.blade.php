@extends('template.index')
@section('content')
<style>
	table tr th {
		font-size: 10px;
		text-align: center;
	}

	table tr td {
		font-size: 9px;
		text-align: center;

	}
	table.dataTable thead th{
		text-align: center;
	}

	.break {
		word-wrap: break-word;
		white-space: pre;
	}
</style>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">

				<div class="card-body ">
					<div class="row mb-4">
						<div class="col-sm-3">
							<di class="row">
								<div class="col-sm-12">Rincian Data</div>
							</di>
							<di class="row">
								<div class="col-sm-4">Kecamatan</div>
								<div class="col-sm-4" id="kecamatan">: {{ $kecamatan }}</div>
								<input type="hidden" id="trx" value="{{ $detail->id_transaksi_master }}">
							</di>
							<di class="row">
								<div class="col-sm-4">Bulan</div>
								<div class="col-sm-4" id="bulan-detail">: {{ $this->CI->access->BulanIndo($detail->bulan) }}</div>
							</di>
							<di class="row">
								<div class="col-sm-4">Tahun</div>
								<div class="col-sm-6" id="tahun-detail">: {{ $detail	->tahun }}</div>
							</di>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-bordered " id="tb-data" width="100%">
							<thead class="bg-info text-white">
								<tr>
									<th rowspan="2" class="break">
										No
									</th>
									<th rowspan="2" class="break">
										Jenis Ternak
									</th>
									<th colspan="3">Populasi Lokal</th>
									<th rowspan="2" class="break">
										Populasi SP
									</th>
									
									<th colspan="3">Produksi</th>
									<th rowspan="2" class="break">
										Pemotongan
									</th>
									<th rowspan="2" class="break">
										Pemasukan
									</th>
									<th rowspan="2" class="break">
										Pengeluaran
									</th>
									<th rowspan="2" class="break">
										Kelahiran
									</th>
									<th rowspan="2" class="break">
										Kematian
									</th>
								</tr>
								<tr>
									<th>A</th>
									<th>M</th>
									<th>D</th>
									<th>Daging (Kg)</th>
									<th>Susu (ltr)</th>
									<th>Telur (btr)</th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
@include('transaksi.add')
@include('transaksi.edit')
<script>
	const monthNames = ["Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agusturs", "September",
		"Oktober", "November", "Desember"
	];
	const years = [];
	const months = [];
	const currentYear = (new Date()).getFullYear();
	const currentMonth = new Date().getMonth();
	$(function() {
		ajaxcsrf();
		let id_trx = '{{ $detail->id_transaksi_master }}';
		Datatables();

		function Datatables() {
			$('#tb-data').DataTable({
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
				},
				stateSave: true,
				destroy: true,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					"url": base_url + "dns_perternakan/transaksi/tables-view",
					"type": "POST",
					"data": {
						csrf_hash_name: $('meta[name="csrf-token"]').attr("content"),
					},
					"data": function(data) {
						data.csrf_hash_name = $('meta[name="csrf-token"]').attr("content");
						data.transaksi = $('#trx').val();
					},
					"dataSrc": function(response) {
						$('meta[name="csrf-token"]').attr("content", response.csrf_param);
						return response.data;
					},

				},
				"columns": [{
						data: "no"
					},
					{
						data: {
							nama: "nama",
							data_trx: "data_trx",
							id_komoditi: "id_komoditi"
						},
						render: function(d) {
							let btn = '<button class="btn btn-xs btn-info add" data-id="' + id_trx + '" data-kom="' + d.id_komoditi + '">Input Data</button>';
							if (d.data_trx.success) {
								btn = '<button class="btn btn-xs btn-warning edit" data-id="' + id_trx + '" data-kom="' + d.id_komoditi + '">Ubah Data</button>';
							}

							return '<b style="font-size:11px;">' + d.nama + '</b> <br>' + btn
						}
					},
					{
						data: 'data_plokal',
						render: function(d) {
							if (d.success) {
								for (x in d.data) {
									if (d.data[x].kategori == 'P-A') {
										return d.data[x].total
									}

								}

							} else {
								return "0"
							}
						}
					},
					{
						data: 'data_plokal',
						render: function(d) {
							if (d.success) {
								for (x in d.data) {
									if (d.data[x].kategori == 'P-M') {
										return d.data[x].total
									}

								}

							} else {
								return "0"
							}
						}
					},
					{
						data: 'data_plokal',
						render: function(d) {
							if (d.success) {
								for (x in d.data) {
									if (d.data[x].kategori == 'P-D') {
										return d.data[x].total
									}

								}

							} else {
								return "0"
							}
						}
					},
					{
						data: 'data_trx',
						render: function(d) {
							if (d.success) {
								for (x in d.data) {
									return d.data[x].populasi_sp
								}

							} else {
								return "0"
							}
						}
					},
					{
						data: 'data_prod',
						render: function(d) {
							if (d.success) {
								for (x in d.data) {
									return d.data[x].produksi
								}

							} else {
								return "0"
							}
						}
					},
					{
						data: 'data_prod',
						render: function(d) {
							if (d.success) {
								for (x in d.data) {
									return d.data[x].susu
								}

							} else {
								return "0"
							}
						}
					},
					{
						data: 'data_prod',
						render: function(d) {
							if (d.success) {
								for (x in d.data) {
									return d.data[x].telur
								}

							} else {
								return "0"
							}
						}
					},
					{
						data: 'data_prod',
						render: function(d) {
							if (d.success) {
								for (x in d.data) {
									return d.data[x].pemotongan
								}

							} else {
								return "0"
							}
						}
					},
					{
						data: 'data_trx',
						render: function(d) {
							if (d.success) {
								for (x in d.data) {
									return d.data[x].pemasukan
								}

							} else {
								return "0"
							}
						}
					},
					{
						data: 'data_trx',
						render: function(d) {
							if (d.success) {
								for (x in d.data) {
									return d.data[x].pengeluaran
								}

							} else {
								return "0"
							}
						}
					},
					{
						data: 'data_trx',
						render: function(d) {
							if (d.success) {
								for (x in d.data) {
									return d.data[x].kelahiran
								}

							} else {
								return "0"
							}
						}
					},
					{
						data: 'data_trx',
						render: function(d) {
							if (d.success) {
								for (x in d.data) {
									return d.data[x].kematian
								}

							} else {
								return "0"
							}
						}
					},
				],
				scrollY: '100vh',
				scroller: {
					loadingIndicator: true
				},


			});
		}

		$('#tb-data').on('click', '.add', function() {
			let sid = $(this).data('id');
			let idkm = $(this).data('kom');
			$('#form-add-transaksi')[0].reset();
			$('[name="mode"]').val('add');
			$('#id_komoditi').val(idkm).trigger('change');
			$.ajax({
				url: base_url + 'dns_perternakan/transaksi/detail-data',
				type: 'GET',
				dataType: 'JSON',
				data: {
					sid: sid,
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				success: function(response) {
					$('#id_komoditi').val(idkm).trigger('change');
					$('[name="id_komoditi"]').val(idkm);
					$('#tahun').val(response.data.tahun).trigger('change');
					$('#bulan').val(response.data.bulan).trigger('change');
					DetailData(sid, idkm)
				}
			})


			$('#add-modal').modal('show');
		})

		$('#tb-data').on('click', '.edit', function() {
			let sid = $(this).data('id');
			let idkm = $(this).data('kom');
			$('[name="mode"]').val('edit');
			$('#id_komoditi').val(idkm).trigger('change');
			$.ajax({
				url: base_url + 'dns_perternakan/transaksi/detail-data',
				type: 'GET',
				dataType: 'JSON',
				data: {
					sid: sid,
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				success: function(response) {
					$('#id_komoditi').val(idkm).trigger('change');
					$('[name="id_komoditi"]').val(idkm);
					$('#tahun').val(response.data.tahun).trigger('change');
					$('#bulan').val(response.data.bulan).trigger('change');
					DetailData(sid, idkm)
				}
			})


			$('#add-modal').modal('show');
		})
		for (var i = currentYear; i >= 2015; i--) {
			years.push({
				'id': i,
				'text': i
			});
		}

		for (var month = 0; month <= 11; month++) {
			months.push({
				'id': month + 1,
				'text': monthNames[month]
			});
		}

		$('#tahun').select2({
			width: '100%',
			theme: 'bootstrap4',
			allowClear: true,
			placeholder: '==Pilih tahun==',
			data: years
		});

		$('#bulan').select2({
			width: '100%',
			theme: 'bootstrap4',
			allowClear: true,
			placeholder: '==Pilih Bulan==',
			data: months
		});
		$("#kakas, #pemotongan").keyup(function() {
			let kakas = parseInt($("#kakas").val()) || 0
			let pemotongan = parseInt($("#pemotongan").val()) || 0
			let total_prod = kakas * pemotongan
			$("#produksi_daging").val(total_prod.toFixed(0));
		});

		$("#jumlah_pa, #kelahiran_pa,#kematian_pa").keyup(function() {
			let jumlah_pa = parseInt($("#jumlah_pa").val()) || 0;
			let lahir_pa = parseInt($("#kelahiran_pa").val()) || 0;
			let mati_pa = parseInt($("#kematian_pa").val()) || 0;
			let total_pol = jumlah_pa + lahir_pa - mati_pa;
			$("#total_pa").val(total_pol.toFixed(0));
		});

		$("#jumlah_pm, #kematian_pm").keyup(function() {
			let jumlah_pm = parseInt($("#jumlah_pm").val()) || 0;
			let mati_pm = parseInt($("#kematian_pm").val()) || 0;
			let total_pol = jumlah_pm - mati_pm;
			$("#total_pm").val(total_pol.toFixed(0));
		});

		$("#jumlah_pd, #kematian_pd").keyup(function() {
			let jumlah_pd = parseInt($("#jumlah_pd").val()) || 0;
			let mati_pd = parseInt($("#kematian_pd").val()) || 0;
			let total_pol = jumlah_pd - mati_pd;
			$("#total_pd").val(total_pol.toFixed(0));

			let pemasukan = parseInt($("#pemasukan").val()) || 0;
			let pengeluaran = parseInt($("#pengeluaran").val()) || 0;
			let total_psp = total_pol + pemasukan - pengeluaran;
			$("#populasi_sp").val(total_psp.toFixed(0));
		});

		$('.kelahiran_jml').keyup(function() {
			var sum = 0;
			$(".kelahiran_jml").each(function() {
				let value = parseInt($(this).val()) || 0;
				sum += value;
			});

			$("#kelahiran_ttl").val(sum);
		})

		$('.kematian_jml').keyup(function() {
			var sum = 0;
			$(".kematian_jml").each(function() {
				let value = parseInt($(this).val()) || 0;
				sum += value;
			});

			$("#kematian_ttl").val(sum);
		})

		$('#populasi_sp').on('click', function() {
			let total_pol = parseInt($("#total_pd").val()) || 0;
			let pemasukan = parseInt($("#pemasukan").val()) || 0;
			let pengeluaran = parseInt($("#pengeluaran").val()) || 0;
			let total_psp = total_pol + pemasukan - pengeluaran;
			$("#populasi_sp").val(total_psp.toFixed(0));
		})
		$('form#form-add-transaksi').submit(function(e) {
			e.preventDefault();
			e.stopImmediatePropagation();
			let FormData = $(this).serialize() + "&csrf_hash_name=" + $('meta[name="csrf-token"]').attr("content");
			$.ajax({
				url: base_url + 'dns_perternakan/transaksi/create-data',
				type: 'POST',
				dataType: 'JSON',
				data: FormData,
				success: function(response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					$('#add-modal').modal('hide');
					Datatables()
				}
			})


		});


		DataKomoditi()

		function DataKomoditi() {
			let komoditi = [];
			$.ajax({
				url: base_url + 'dns_perternakan/komoditi/data-tables',
				type: 'GET',
				dataType: 'JSON',
				success: function(response) {
					for (d in response.data) {
						komoditi.push({
							'id': response.data[d].id_komoditi,
							'text': response.data[d].nama
						});
					}
					$('#id_komoditi').select2({
						width: '100%',
						theme: 'bootstrap4',
						allowClear: true,
						placeholder: '==Pilih Ternak==',
						data: komoditi
					});
				}
			})
		}
		dataKecamatan()

		function dataKecamatan() {
			let opsi_kecamatan = [];
			$.ajax({
				url: base_url + 'master/data-kecamatan',
				type: 'GET',
				data: {
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				dataType: "json",
				success: function(response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					for (key in response.data) {
						let id_ = response.data[key].id_kecamatan
						let name = response.data[key].nama_kecamatan
						opsi_kecamatan.push({
							'id': id_,
							'text': name
						});

					}
					$('#id_kecamatan').select2({
						width: '100%',
						theme: 'bootstrap4',
						allowClear: true,
						placeholder: '==Pilih Kecamatan==',
						data: opsi_kecamatan
					});

				}
			});
		}

		function DetailData(idtrx, idkm) {
			$.ajax({
				url: base_url + 'dns_perternakan/transaksi/detail-edit',
				type: 'GET',
				dataType: 'JSON',
				data: {
					sid: idtrx,
					idkm: idkm,
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				success: function(response) {
					console.log(response);
					let daging = (response.data.data_kom.daging == 'Y' ? true : false);
					let susu = (response.data.data_kom.susu == 'Y' ? true : false);
					let telur = (response.data.data_kom.telur == 'Y' ? true : false);
					if (daging) {
						$('#p-daging').show('fast');
					} else {
						$('#p-daging').hide('fast');
					}

					if (susu) {
						$('#p-susu').show('fast');
					} else {
						$('#p-susu').hide('fast');
					}

					if (telur) {
						$('#p-telur').show('fast');
					} else {
						$('#p-telur').hide('fast');
					}



					for (a in response.data.data_trx) {
						$('#pemasukan').val(response.data.data_trx[a].pemasukan);
						$('#pengeluaran').val(response.data.data_trx[a].pengeluaran);
						$('#kematian_ttl').val(response.data.data_trx[a].kematian);
						$('#kelahiran_ttl').val(response.data.data_trx[a].kelahiran);
						$('#populasi_sp').val(response.data.data_trx[a].populasi_sp);
					}

					for (b in response.data.data_plokal) {

						let name_ = response.data.data_plokal[b].kategori;
						let jumlah = response.data.data_plokal[b].jumlah;
						let total = response.data.data_plokal[b].total;
						let lahir = response.data.data_plokal[b].lahir;
						let mati = response.data.data_plokal[b].mati;
						$('[name="jumlah[' + name_ + ']"]').val(jumlah);
						$('[name="total[' + name_ + ']"]').val(total);
						$('[name="lahir[' + name_ + ']"]').val(lahir);
						$('[name="mati[' + name_ + ']"]').val(mati);
					}

					for (c in response.data.data_prod) {
						$('#kakas').val(response.data.data_prod[c].kakas);
						$('#pemotongan').val(response.data.data_prod[c].pemotongan);
						$('#produksi_daging').val(response.data.data_prod[c].produksi);

					}
				}
			})
		}

	})
</script>


@endsection