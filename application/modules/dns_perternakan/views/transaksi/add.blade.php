<div id="add-modal" class="modal fade" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="title-form"></h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="form-add-transaksi" class="form-horizontal r-separator">
				<input type="hidden" name="id_trx" value="{{ $sid }}">
				<input type="hidden" name="mode" value="">
				<div class="modal-body">
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Kecamatan</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<select class="form-control" id="id_kecamatan" disabled="">
							</select>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Jenis Ternak</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="hidden" name="id_komoditi">
							<select class="form-control" id="id_komoditi" disabled="">

							</select>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Bulan</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<select class="form-control" name="bulan" id="bulan" disabled="">

							</select>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Tahun</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<select class="form-control" name="tahun" id="tahun" disabled="">

							</select>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Pemotongan</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control produksi[]" name="pemotongan" id="pemotongan" placeholder="Pemotongan" required>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Produksi</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<div class="row" id="p-daging">
								<div class="col-sm-12">
									<label for="">Kakas</label>
									<input type="text" class="form-control produksi" name="kakas" id="kakas" placeholder="Kakas" >
								</div>
								
								<div class="col-sm-12">
									<hr>
									<label for="">Produksi Daging (Kg)</label>
									<input type="text" class="form-control" name="produksi[daging]" id="produksi_daging" placeholder="0" readonly="">
									<small>produksi Daging (kg) = kakas * pemotongan</small>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12" id="p-susu">
									<label for="">Produksi Susu</label>
									<input type="text" class="form-control" name="produksi[susu]" id="produksi_susu" placeholder="0">
								</div>
								<div class="col-sm-12" id="p-telur">
									<label for="">Produksi Telur</label>
									<input type="text" class="form-control" name="produksi[telur]" id="produksi_telur" placeholder="0">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Pemasukan</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" name="pemasukan" id="pemasukan" placeholder="Pemasukan" required>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Pengeluaran</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" name="pengeluaran" id="pengeluaran" placeholder="Pengeluaran" required>
						</div>
					</div>

					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Populasi Lokal</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<div class="row">
								<div class="col-sm-4">
									<label for="">Anak</label>
									<input type="text" class="form-control " name="jumlah[P-A]" id="jumlah_pa" placeholder="Jumlah" required>

								</div>
								<div class="col-sm-4">
									<label for="">Kelahiran</label>
									<input type="text" class="form-control kelahiran_jml" name="lahir[P-A]" id="kelahiran_pa" placeholder="Kelahiran" required>

								</div>
								<div class="col-sm-4">
									<label for="">Kematian</label>
									<input type="text" class="form-control kematian_jml" name="mati[P-A]" id="kematian_pa" placeholder="Kematian" required>
								</div>
								<div class="col-sm-12">
									<label for="">Total Populasi</label>
									<input type="text" class="form-control" name="total[P-A]" id="total_pa" placeholder="0" readonly>
									<small>total populasi = jumlah + kelahiran - kematian</small>
								</div>
							</div>

							<hr>

							<div class="row">
								<div class="col-sm-4">
									<label for="">Muda</label>
									<input type="text" class="form-control" name="jumlah[P-M]" id="jumlah_pm" placeholder="Jumlah" required>

								</div>
								<div class="col-sm-4">
									<label for="">Kelahiran</label>
									<input type="text" class="form-control kelahiran_jml" name="lahir[P-M]" id="kelahiran_pm" placeholder="Kelahiran" readonly value="0">
								</div>
								<div class="col-sm-4">
									<label for="">Kematian</label>
									<input type="text" class="form-control kematian_jml" name="mati[P-M]" id="kematian_pm" placeholder="Kematian" required>
								</div>
								<div class="col-sm-12">
									<label for="">Total Populasi</label>
									<input type="text" class="form-control" name="total[P-M]" id="total_pm" placeholder="0" readonly>
									<small>total populasi = jumlah - kematian</small>
								</div>
							</div>
							<hr>

							<div class="row">
								<div class="col-sm-4">
									<label for="">Dewasa</label>
									<input type="text" class="form-control" name="jumlah[P-D]" id="jumlah_pd" placeholder="Jumlah" required>
								</div>
								<div class="col-sm-4">
									<label for="">Kelahiran</label>
									<input type="text" class="form-control" name="lahir[P-D]" id="kelahiran_pd" placeholder="Kelahiran" readonly value="0">
								</div>
								<div class="col-sm-4">
									<label for="">Kematian</label>
									<input type="text" class="form-control kematian_jml" name="mati[P-D]" id="kematian_pd" placeholder="Kematian" required>
								</div>
								<div class="col-sm-12">
									<label for="">Total Populasi</label>
									<input type="text" class="form-control" name="total[P-D]" id="total_pd" placeholder="0" readonly>
									<small>total populasi = jumlah - kematian</small>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Kelahiran</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" name="kelahiran" id="kelahiran_ttl" placeholder="0" readonly>
							<small>Total Kelahiran Populasi Anak</small>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Kematian</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" name="kematian" id="kematian_ttl" placeholder="0" readonly>
							<small>Total Kematian populasi Anak + populasi Muda + populasi Dewasa</small>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Populasi Siap Pakai</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" name="populasi_sp" id="populasi_sp" placeholder="0" readonly>
							<small>populasi lokal dewasa + pemasukan – pengeluaran</small>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-danger waves-effect waves-light btn-simpan">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>