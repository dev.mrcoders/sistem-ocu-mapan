@extends('template.index')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">

				<div class="card-body ">
					<div class="row mb-4">
						<div class="col-sm-3">
							<h4 class="card-title">Data Komoditi</h4>
						</div>
						<div class="col-sm-9 text-right">
							<button type="button" class="btn btn-sm waves-effect waves-light btn-outline-primary add"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</button>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table nowrap" id="tb-data" width="100%">
							<thead class="bg-info text-white">
								<tr>
									<th>No</th>
									<th>Bulan</th>
									<th>Tahun</th>
									<th>Kecamatan</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
@include('transaksi.generate')
@include('komoditi.edit')
<script>
	$(function() {
		ajaxcsrf();
		Datatables();
		const monthNames = ["Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agusturs", "September",
			"Oktober", "November", "Desember"
		];
		const years = [];
		const months = [];
		const currentYear = (new Date()).getFullYear();
		const currentMonth = new Date().getMonth();

		function Datatables() {
			$('#tb-data').DataTable({
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
				},
				stateSave: true,
				destroy: true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					"url": base_url + "dns_perternakan/transaksi/data-tables",
					"type": "POST",
					"data": {
						csrf_hash_name: $('meta[name="csrf-token"]').attr("content"),
					},
					"data": function(data) {
						data.csrf_hash_name = $('meta[name="csrf-token"]').attr("content");
					},
					"dataSrc": function(response) {
						$('meta[name="csrf-token"]').attr("content", response.csrf_param);
						return response.data;
					},

				},
				"columns": [{
						data: "no"
					},
					{
						data: 'bulan',
						render: function(d) {
							return BlnIndo(d);
						}
					},
					{
						data: 'tahun',
					},
					{
						data: 'kecamatan',
					},
				],
				columnDefs: [{
					targets: 4,
					data: "sid",
					render: function(data) {
						return '<div role="group" class="btn-group-sm btn-group btn-group-toggle" data-toggle="buttons">' +
							'<button class="btn btn-warning lihat" data-id="' + data +
							'">Lihat Data</button>' +
							'<button class="btn btn-danger hapus" data-id="' + data +
							'">Hapus</button>' +
							'</div>';
					}
				}]

			});
		}

		$('#tb-data').on('click', '.lihat', function() {
			let sid = $(this).data('id');
			window.location.href = base_url + 'dns_perternakan/transaksi/lihat-data/' + sid
		})

		$('.add').on('click', function() {
			$('#id_kecamatan,#bulan,#tahun').val(null).change()
			$('#generate-modal').modal('show');
		})

		for (var i = currentYear; i >= 2015; i--) {
			years.push({
				'id': i,
				'text': i
			});
		}

		for (var month = 0; month <= 11; month++) {
			months.push({
				'id': month + 1,
				'text': monthNames[month]
			});
		}

		$('#tahun').select2({
			width: '100%',
			theme: 'bootstrap4',
			allowClear: true,
			placeholder: '==Pilih tahun==',
			data: years
		});

		$('#bulan').select2({
			width: '100%',
			theme: 'bootstrap4',
			allowClear: true,
			placeholder: '==Pilih Bulan==',
			data: months
		});

		$("#form-generat-data").validate({
			rules: {
				"id_kecamatan": "required",
				"bulan": "required",
				"tahun": "required",

			},
			messages: {
				"id_kecamatan": "Wajib Di isi",
				"bulan": "Wajib Di isi",
				"tahun": "Wajib Di isi",
			},
			errorElement: "em",
			errorPlacement: function(error, element) {
				// Add the `invalid-feedback` class to the error element
				error.addClass("invalid-feedback");
				if (element.hasClass("select2")) {
					error.insertAfter(element.next("span"));
				} else {
					error.insertAfter(element);
				}
			},
			highlight: function(element, errorClass, validClass) {
				$(element).addClass("is-invalid").removeClass("is-valid");
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).addClass("is-valid").removeClass("is-invalid");
			},
			submitHandler: submitData,
		});

		function submitData(){
			let FormData = $('#form-generat-data').serialize() + "&csrf_hash_name=" + $('meta[name="csrf-token"]').attr("content");
			$.ajax({
				type: "post",
				url: base_url+"dns_perternakan/transaksi/generate-data",
				data: FormData,
				dataType: "JSON",
				success: function (response) {
					console.log(response)
				}
			});
		}

		function BlnIndo(bln) {
			let bulan = {
				1: 'Januari',
				2: 'Februari',
				3: 'Maret',
				4: 'April',
				5: 'Mei',
				6: 'Juni',
				7: 'Juli',
				8: 'Agustus',
				9: 'September',
				10: 'Oktober',
				11: 'November',
				12: 'Desember'
			};

			return bulan[bln];
		}

		dataKecamatan()

		function dataKecamatan() {
			let opsi_kecamatan = [];
			$.ajax({
				url: base_url + 'master/data-kecamatan',
				type: 'GET',
				data: {
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				dataType: "json",
				success: function(response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					for (key in response.data) {
						let id_ = response.data[key].id_kecamatan
						let name = response.data[key].nama_kecamatan
						opsi_kecamatan.push({
							'id': id_,
							'text': name
						});

					}
					$('#id_kecamatan').select2({
						width: '100%',
						theme: 'bootstrap4',
						allowClear: true,
						placeholder: '==Pilih Kecamatan==',
						data: opsi_kecamatan
					});

				}
			});
		}

	})
</script>


@endsection