<div id="generate-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Generate Form Data Baru</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form id="form-generat-data" class="form-horizontal r-separator">
                <div class="modal-body">
                    <div class="form-group row align-items-center m-b-0">
                        <label for="inputEmail3" class="col-3 text-right control-label col-form-label">Kecamatan</label>
                        <div class="col-9 border-left p-b-10 p-t-10">
                            <select class="form-control" id="id_kecamatan" name="id_kecamatan">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row align-items-center m-b-0">
                        <label for="inputEmail3" class="col-3 text-right control-label col-form-label">Bulan</label>
                        <div class="col-9 border-left p-b-10 p-t-10">
                            <select class="form-control" name="bulan" id="bulan">

                            </select>
                        </div>
                    </div>
                    <div class="form-group row align-items-center m-b-0">
                        <label for="inputEmail3" class="col-3 text-right control-label col-form-label">Tahun</label>
                        <div class="col-9 border-left p-b-10 p-t-10">
                            <select class="form-control" name="tahun" id="tahun">

                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger waves-effect waves-light btn-simpan">Generate Dan Show</button>
                </div>
            </form>
        </div>
    </div>
</div>