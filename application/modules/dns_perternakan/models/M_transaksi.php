<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_transaksi extends CI_Model {
	var $disbun_db;
	var $table = 'tb_transaksi_master';
	var $primary_key= 'id_transaksi_master';
	var $column_order = array(null, 'id_kecamatan','tahun','bulan');
	var $column_search = array('tahun');
	var $order = array('id_transaksi_master' => 'asc');

	public function __construct()
	{
		parent::__construct();
		$this->disbun_db = $this->load->database('disnak',TRUE);
	}

	private function _get_datatables_query()
	{
		$this->disbun_db->from($this->table);

		$i = 0;
		foreach ($this->column_search as $item) 
		{
			if ($this->input->post('search')['value']) 
			{
				if ($i === 0) 
				{
					$this->disbun_db->group_start();
					$this->disbun_db->like($item, $this->input->post('search')['value']);
				}else{
					$this->disbun_db->or_like($item, $this->input->post('search')['value']);
				}
				if (count($this->column_search) - 1 == $i) 
					$this->disbun_db->group_end();
			}
			$i++;
		}

		if ($this->input->post('order')) {
			$this->disbun_db->order_by($this->column_order[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->disbun_db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if ($this->input->post('length') != -1)
			$this->disbun_db->limit($this->input->post('length'), $this->input->post('start'));
		$query = $this->disbun_db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->disbun_db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->disbun_db->from($this->table);
		return $this->disbun_db->count_all_results();
	}

	public function Saved($tables,$data,$batch=false)
	{
		if($batch){
			return $this->disbun_db->insert_batch($tables, $data);
		}else{
			return $this->disbun_db->insert($tables,$data);
		}
		
	}

	public function Updated($tables,$data,$idtrx,$batch=false)
	{
		if($batch){
			$this->disbun_db->where('id_transaksi_master', $idtrx);
			if($tables == 'tb_transaksi_populasi_lkl'){
				$this->disbun_db->where('id_komoditi', $data['id_komoditi']);
				return $this->disbun_db->update_batch($tables, $data, 'kategori');
			}else{
				return $this->disbun_db->update_batch($tables, $data, 'id_komoditi');
			}
			
		}else{
			$this->disbun_db->where('id_transaksi_master',$idtrx);
			return $this->disbun_db->update($tables,$data);
		}
		
	}

	

	public function GetById($id=null)
	{
		if($this->input->get('sid')){
			$this->disbun_db->where($this->primary_key,$this->input->get('sid'));
		}
		if($id != null){
			$this->disbun_db->where($this->primary_key,$id);
		}
		
		$trx = $this->disbun_db->get($this->table);
		return ($this->input->get('sid') ? $trx->row():($id != null ? $trx->row():$trx->result()));
	}

	public function Deleted($id)
	{
		$this->disbun_db->where($this->primary_key,$id);
		return $this->disbun_db->delete($this->table);
	}


	public function GetTrxData($table,$data)
	{
		$trx = $this->disbun_db->get_where($table,$data)->result_array();

		return $trx;
	}

}

/* End of file M_komoditi.php */
/* Location: ./application/modules/dns_perkebunan/models/M_komoditi.php */