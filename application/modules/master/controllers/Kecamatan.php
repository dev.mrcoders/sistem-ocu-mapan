<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan extends MY_Controller {
	var $csrf_name = '';
	var $csrf_hash;
	var $json = [];
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_kecamatan');
		$this->csrf_name = 'csrf_param';
		$this->csrf_hash = $this->security->get_csrf_hash();
	}

	public function GetDataKecamatan()
	{
		$get_kecamatan = $this->M_kecamatan->GetKecamatan();

		$this->json = array(
			"success" => true,
			"message" => "OK",
			"data" => $get_kecamatan,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

}

/* End of file Kecamatan.php */
/* Location: ./application/modules/master/controllers/Kecamatan.php */