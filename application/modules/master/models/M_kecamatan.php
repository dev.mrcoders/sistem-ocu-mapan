<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kecamatan extends CI_Model {

	var $table = 'tb_kecamatan';
	var $primary_key= 'id_kecamatan';
	var $column_order = array(null,'kode_kecamatan','nama_kecamatan');
	var $column_search = array('kode_kecamatan','nama_kecamatan');
	var $order = array('id_kecamatan' => 'asc');	

	public function GetKecamatan()
	{
		if($this->input->get('sid')){
			$this->db->where('id_kecamatan', $this->input->get('sid'));
		}
		$trx = $this->db->get($this->table);
		return ($this->input->get('sid') ? $trx->row():$trx->result());
	}


	public function GetKecamatanById($sid)
	{

		$this->db->where('id_kecamatan', $sid);
		
		$trx = $this->db->get($this->table);
		return  $trx->row();
	}

}

/* End of file M_kecamatan.php */
/* Location: ./application/modules/master/models/M_kecamatan.php */