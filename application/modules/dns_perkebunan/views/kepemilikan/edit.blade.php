<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Ubah Kepemilikan</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="form-edit-kepemilikan">
				<div class="modal-body">
					<input type="hidden" name="id_kepemilikan" id="id_kepemilikan">
					<div class="form-group">
						<label for="recipient-name" class="control-label">Nama Kepemilikan:</label>
						<input type="text" class="form-control" id="edit_kepemilikan" name="kepemilikan" required>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label">keterangan:</label>
						<input type="text" class="form-control" id="edit_keterangan" name="keterangan" required>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-danger waves-effect waves-light btn-simpan">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>