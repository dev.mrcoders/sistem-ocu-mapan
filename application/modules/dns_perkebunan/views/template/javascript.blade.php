
<!-- Bootstrap tether Core JavaScript -->
<script src="@asset('/')assets/libs/popper.js/dist/umd/popper.min.js"></script>
<script src="@asset('/')assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- apps -->
<script src="@asset('/')dist/js/app.min.js"></script>
<script src="@asset('/')dist/js/app.init.mini-sidebar.js"></script>
<script src="@asset('/')dist/js/app-style-switcher.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="@asset('/')assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
<script src="@asset('/')assets/extra-libs/sparkline/sparkline.js"></script>
<!--Wave Effects -->
<script src="@asset('/')dist/js/waves.js"></script>
<!--Menu sidebar -->
<script src="@asset('/')dist/js/sidebarmenu.js"></script>
<!--Custom JavaScript -->
<script src="@asset('/')dist/js/custom.min.js"></script>
<!--This page JavaScript -->

{{-- <script src="@asset('/')dist/js/pages/dashboards/dashboard1.js"></script> --}}
{{-- js datatables --}}


<script src="@asset('/')assets/extra-libs/data-tables/datatables.min.js"></script>

<script src="@asset('node_modules/')sweetalert2/dist/sweetalert2.min.js"></script>
<script src="@asset('/')assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="@asset('/')assets/libs/select2/dist/js/select2.full.min.js"></script>
<script src="@asset('/')assets/libs/select2/dist/js/select2.min.js"></script>
<script>
	var csrfname = '{{ get_csrf_token_name() }}';
	var csrfhash = '{{ get_csrf_hash() }}';
	$('meta[name="csrf-param"]').attr("content", csrfname);
	$('meta[name="csrf-token"]').attr("content", csrfhash);
	$('[data-toggle="tooltip"]').tooltip();
	$(".preloader").fadeOut();
	const base_url = '{{ base_url('') }}';

	function ajaxcsrf() {
		var csrf = {};
		csrf[csrfname] = csrfhash;
		$.ajaxSetup({
			"data": csrf
		});
	}

	function update_csrf_fields(value) {
		$('[name="csrf_hash_name"]').val(value);
	}
</script>
<script>
	$(function(){
		$(document).ajaxComplete(function(event, xhr, settings) {
			// console.log(xhr);
		});
	})
	
</script>
