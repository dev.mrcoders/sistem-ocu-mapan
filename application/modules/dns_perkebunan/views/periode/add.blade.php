<div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah Periode Baru</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="form-add-periode">
				<div class="modal-body">

					<div class="form-group">
						<label for="recipient-name" class="control-label">Semester:</label>
						<input type="text" class="form-control" id="semester" name="semester" required>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label">Tahun:</label>
						<input type="text" class="form-control" id="tahun" name="tahun" required>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-danger waves-effect waves-light btn-simpan">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>