@extends('template.index')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">

				<div class="card-body ">
					<div class="row mb-4">
						<div class="col-sm-3">
							<h4 class="card-title">Data Komoditi</h4>
						</div>
						<div class="col-sm-9 text-right">
							<button type="button" class="btn btn-sm waves-effect waves-light btn-outline-primary add"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</button>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table nowrap" id="tb-data" width="100%">
							<thead class="bg-info text-white">
								<tr>
									<th>No</th>
									<th>Semester</th>
									<th>Tahun</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
@include('periode.add')
@include('periode.edit')
<script>
	$(function(){
		ajaxcsrf();
		Datatables();
		function Datatables() {
			$('#tb-data').DataTable({
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
				},
				stateSave: true,
				destroy: true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					"url": base_url + "dns_perkebunan/periode/data-tables",
					"type": "POST",
					"data": {
						csrf_hash_name: $('meta[name="csrf-token"]').attr("content"),
					},
					"data": function(data) {
						data.csrf_hash_name = $('meta[name="csrf-token"]').attr("content");
					},
					"dataSrc": function(response) {
						$('meta[name="csrf-token"]').attr("content", response.csrf_param);
						return response.data;
					},

				},
				"columns": [{
					data:"no"
				},
				{
					data: 'semester',
				},
				{
					data: 'tahun',
				},
				],
				columnDefs: [{
					targets: 3,
					data: "id_periode",
					render: function(data) {
						return '<div role="group" class="btn-group-sm btn-group btn-group-toggle" data-toggle="buttons">' +
						'<button class="btn btn-warning edit" data-id="' + data +
						'">Ubah</button>' +
						'<button class="btn btn-danger hapus" data-id="' + data +
						'">Hapus</button>' +
						'</div>';
					}
				}]
				
			});
		}

		$('.add').on('click',function(){
			$('#form-add-periode')[0].reset();
			$('#form-add-periode .invalid-feedback').remove()
			$('#form-add-periode input, select').removeClass('is-valid');
			$('#form-add-periode input, select').removeClass('is-invalid');
			$('#add-modal').modal('show')
		})

		$('#tb-data').on('click','.edit',function(){
			let sid = $(this).data('id');
			data_periode(sid)
			$('#edit-modal').modal('show')
			
		})

		$('#tb-data').on('click','.hapus',function(){
			let sid = $(this).data('id');
			
			Swal.fire({
				title: 'Are you sure?',
				text: "Data Tidak Dapat Dipulihkan",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			}).then((result) => {
				if (result.isConfirmed) {
					deletePeriode(sid)
				}
			})
			
		})

		$("#form-add-periode").validate({
			rules: {
				"semester": "required",
				"tahun": "required",

			},
			messages: {
				"semester": "Wajib Di isi",
				"tahun": "Wajib Di isi",
			},
			errorElement: "em",
			errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
				error.addClass("invalid-feedback");
				if (element.hasClass("select2")) {
					error.insertAfter(element.next("span"));
				} else {
					error.insertAfter(element);
				}
			},
			highlight: function(element, errorClass, validClass) {
				$(element).addClass("is-invalid").removeClass("is-valid");
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).addClass("is-valid").removeClass("is-invalid");
			},
			submitHandler: submitPeriode,
		});

		$("#form-edit-periode").validate({
			rules: {
				"semester": "required",
				"tahun": "required",

			},
			messages: {
				"semester": "Wajib Di isi",
				"tahun": "Wajib Di isi",
			},
			errorElement: "em",
			errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
				error.addClass("invalid-feedback");
				if (element.hasClass("select2")) {
					error.insertAfter(element.next("span"));
				} else {
					error.insertAfter(element);
				}
			},
			highlight: function(element, errorClass, validClass) {
				$(element).addClass("is-invalid").removeClass("is-valid");
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).addClass("is-valid").removeClass("is-invalid");
			},
			submitHandler: updatePeriode,
		});

		function submitPeriode() {
			const button = $('.btn-simpan');
			let Form = $('#form-add-periode').serialize()+ "&csrf_hash_name=" + $('meta[name="csrf-token"]').attr("content");
			$.ajax({
				url: base_url + "dns_perkebunan/periode/save",
				type: 'POST',
				data: Form,
				beforeSend: function() {
					button.text("Menyimpan..."); 
					button.attr("disabled", true); 
				},
				success: function(response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					if (response.success) {
						Swal.fire({
							position: 'top-end',
							icon: 'success',
							title: 'Data Berhasil Disimpan',
							showConfirmButton: false,
							timer: 1500
						})
						$('#add-modal').modal('hide');
						Datatables();

					}
				},
				complete: function() {
					button.text("Simpan"); 
					button.attr("disabled", false); 
				},
				error: function(xhr, status, error) {
					toastr.error(status + " " + xhr.status + " " + error);
					console.log(xhr.responseText);
				}
			});
		}

		function updatePeriode() {
			const button = $('.btn-simpan');
			let Form = $('#form-edit-periode').serialize()+ "&csrf_hash_name=" + $('meta[name="csrf-token"]').attr("content");
			$.ajax({
				url: base_url + "dns_perkebunan/periode/save",
				type: 'POST',
				data: Form,
				beforeSend: function() {
					button.text("Menyimpan..."); 
					button.attr("disabled", true); 
				},
				success: function(response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					if (response.success) {
						Swal.fire({
							position: 'top-end',
							icon: 'success',
							title: 'Data Berhasil Diubah',
							showConfirmButton: false,
							timer: 1500
						})
						$('#edit-modal').modal('hide');
						Datatables();

					}
				},
				complete: function() {
					button.text("Simpan"); 
					button.attr("disabled", false); 
				},
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				}
			});
		}

		function deletePeriode(sid){
			$.ajax({
				url: base_url+'dns_perkebunan/periode/delete',
				type: 'post',
				data: {
					sid:sid,
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				success: function (response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					Swal.fire({
						position: 'top-end',
						icon: 'success',
						title: 'Data Berhasil Diubah',
						showConfirmButton: false,
						timer: 1500
					})
					Datatables();
				}
			});
		}

		function data_periode(sid){
			$.ajax({
				url: base_url+'dns_perkebunan/periode/databyid',
				type: 'get',
				data: {sid:sid},
				success: function (response) {
					$('#id_periode').val(response.data['id_periode']);
					$('#edit_tahun').val(response.data['tahun']);
					$('#edit_semester').val(response.data['semester']);
				}
			});
		}
	})
</script>


@endsection