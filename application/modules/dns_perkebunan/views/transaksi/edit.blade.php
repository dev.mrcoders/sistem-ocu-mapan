<div id="edit-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah Data Baru</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="form-edit-transaksi" class="form-horizontal r-separator">
				<input type="hidden" name="id_trx" value="{{ $id_trx }}">
				<div class="modal-body">
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Kecamatan</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<select class="form-control" name="id_kecamatan" id="e-id_kecamatan" disabled>
							</select>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Komoditi</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<select class="form-control" name="id_komoditi" id="e-id_komoditi" disabled="">
								
							</select>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Kepemilikan</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<select class="form-control" name="id_kepemilikan" id="e-id_kepemilikan" disabled="">
								
							</select>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Periode</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<select class="form-control" name="id_periode" id="e-id_periode" disabled="">
								
							</select>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">TBM Semester Lalu</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" id="inputEmail3" placeholder="TBM (ha)">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">TBM Semester Ini Menjadi</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" id="inputEmail3" placeholder="TM (ha)">
							<hr>
							<input type="text" class="form-control" id="inputEmail3" placeholder="TTR (ha)">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">TM Semester Lalu</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" id="inputEmail3" placeholder="TM (ha)">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">TM Semester Ini Menjadi TTR</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" id="inputEmail3" placeholder="TTR (ha)">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">TTR Semester Lalu</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" id="inputEmail3" placeholder="TTR (ha)">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">TTR Semester Ini Menjadi TBM</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" id="inputEmail3" placeholder="TBM (ha)">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Semester Ini Buka Lahan Baru</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" id="inputEmail3" placeholder="Lahan baru (ha)">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Keadaan Semester Ini</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" id="inputEmail3" placeholder="TBM (ha)">
							<hr>
							<input type="text" class="form-control" id="inputEmail3" placeholder="TM (ha)">
							<hr>
							<input type="text" class="form-control" id="inputEmail3" placeholder="TTR (ha)">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Jumlah</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" id="inputEmail3" placeholder="">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Produksi</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" id="inputEmail3" placeholder="Ton">
							<hr>
							<input type="text" class="form-control" id="inputEmail3" placeholder="kg/ha/thn">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0" id="e-input-petani">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Petani KK</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" id="inputEmail3" placeholder="KK">
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-danger waves-effect waves-light btn-simpan">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>