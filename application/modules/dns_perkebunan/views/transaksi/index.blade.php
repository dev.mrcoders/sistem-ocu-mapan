@extends('template.index')
@section('content')
<style>
	table tr th{
		font-size:10px;
		text-align:center;
		word-wrap:break-word;
		white-space: pre;
	}
	/*table tr td{
		font-size:9px;
		text-align:center;
	}*/
	@media (min-width: 992px){
		.modal-lg {
			max-width: 1149px;
		}
	}

	hr{
		margin-top:0.5rem;
	}
	.blockquote, hr {
		margin-bottom: 0.5rem;
	}
	
</style>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body ">
					<div class="row mb-4">
						<div class="col-sm-3">
							<h4 class="card-title">Data Master Transaksi</h4>
						</div>
						<div class="col-sm-9 text-right">
							<button type="button" class="btn btn-sm waves-effect waves-light btn-outline-primary add"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</button>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table nowrap table-bordered" id="tb-data" width="100%">
							<thead class="bg-inverse text-white">
								<tr>
									<th>No</th>
									<th>Periode</th>
									<th>Kepemilikan</th>
									<th>Komoditi</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
@include('transaksi.generate')
<script>
	$(function(){
		let id_kecamatan;
		let id_komoditi;
		let id_kepemilikan;
		let id_periode;
		let datas ={};
		let stats ='add';

		ajaxcsrf();
		Datatables();
		dataKecamatan();
		dataKomoditi()
		dataKepemilikan()
		dataPeriode()
		$("select").select2({
			width: '100%',
			theme: 'bootstrap4',
			allowClear: true,
		});
		function Datatables() {
			$('#tb-data').DataTable({
				"language": {
					"url": base_url+"dist/indonesia.json"
				},
				stateSave: true,
				destroy: true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					"url": base_url + "dns_perkebunan/transaksi/data-tables",
					"type": "POST",
					"data": {
						csrf_hash_name: $('meta[name="csrf-token"]').attr("content"),
					},
					"data": function(data) {
						data.csrf_hash_name = $('meta[name="csrf-token"]').attr("content");
					},
					"dataSrc": function(response) {
						$('meta[name="csrf-token"]').attr("content", response.csrf_param);
						return response.data;
					},

				},
				"columns": [{
					data:"no"
				},
				{
					data: 'id_periode',
					render:function(d){
						return 'semseter '+d.semester+' Tahun '+d.tahun
					}
				},
				{
					data: 'id_kepemilikan',
				},
				{
					data: 'id_komoditi',
				},
				],
				columnDefs: [{
					targets: 4,
					data: "id_trx",
					render: function(data) {
						return '<div role="group" class="btn-group-sm btn-group btn-group-toggle" data-toggle="buttons">' +
						'<button class="btn btn-warning lihat" data-id="' + data +
						'">Lihat</button>' +
						'<button class="btn btn-danger hapus" data-id="' + data +
						'">Hapus</button>' +
						'</div>';
					}
				}]
				
			});
		}

		$('.add').on('click',function(){
			stats ='add';
			$('#form-add-generate')[0].reset();
			$('#add-modal').modal('show')
		})

		$('#tb-data').on('click','.lihat',function(){
			let sid = $(this).data('id');
			window.location.href=base_url+"dns_perkebunan/transaksi/data/"+sid;
			
		})

		$('#tb-data').on('click','.edit',function(){
			let sid = $(this).data('id');
			stats ='edit';
			datas = {};
			// data_kepemilikan(sid)
			$('#edit-modal').modal('show')
			console.log(datas);
			
		})

		$('#tb-data').on('click','.hapus',function(){
			let sid = $(this).data('id');
			
			Swal.fire({
				title: 'Are you sure?',
				text: "Data Tidak Dapat Dipulihkan",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			}).then((result) => {
				if (result.isConfirmed) {
					deleteTransaksi(sid)
				}
			})
			
		})

		// $('#id_kecamatan,#e-id_kecamatan').change(function (e) {
		// 	e.preventDefault();
		// 	id_kecamatan = $(this).val();
		// 	datas['id_kecamatan']=id_kecamatan
		// 	let attr = (stats == 'add'? $('#id_komoditi'):$('#e-id_komoditi'))
		// 	attr.removeAttr('disabled')
		// });

		// $('#id_komoditi,#e-id_komoditi').change(function (e) {
		// 	e.preventDefault();
		// 	id_komoditi = $(this).val();
		// 	datas['id_komoditi']=id_komoditi
		// 	let attr = (stats == 'add'? $('#id_kepemilikan'):$('#e-id_kepemilikan'))
		// 	attr.removeAttr('disabled')
		// });

		// $('#id_kepemilikan,#e-id_kepemilikan').change(function (e) {
		// 	e.preventDefault();
		// 	id_kepemilikan = $(this).val();
		// 	datas['id_kepemilikan']=id_kepemilikan
		// 	let attr = (stats == 'add'? $('#id_periode'):$('#e-id_periode'))
		// 	attr.removeAttr('disabled')
		// });

		// $('#id_periode,#e-id_periode').change(function (e) {
		// 	e.preventDefault();
		// 	id_periode = $(this).val();
		// 	datas['id_periode']=id_periode
		// 	searchDataSmstlalu()
		// });

		$('#tbm-saat-ini').on('click',function(){
			let tbm_lalu=parseInt($('#tbm-lalu').val()) || 0;
			let tbm_lalu_to_tm=parseInt($('#tbm-lalu-to-tm').val()) || 0;
			let tbm_lalu_to_ttr=parseInt($('#tbm-lalu-to-ttr').val()) || 0;
			let ttr_ini_to_tbm =parseInt($('#ttr-ini-to-tbm').val()) || 0;
			let lahan_baru =parseInt($('#lahan-baru').val()) || 0;

			let tbm_saat_ini = tbm_lalu - tbm_lalu_to_tm - tbm_lalu_to_ttr + ttr_ini_to_tbm + lahan_baru;

			$('#tbm-saat-ini').val(tbm_saat_ini);
			
		})

		$('#tm-saat-ini').on('click',function(){
			let tbm_lalu_to_tm=parseInt($('#tbm-lalu-to-tm').val()) || 0;
			let tm_lalu=parseInt($('#tm-lalu').val()) || 0;
			let tm_ini_to_ttr=parseInt($('#tm-ini-to-ttr').val()) || 0;

			let tm_saat_ini = tbm_lalu_to_tm + tm_lalu - tm_ini_to_ttr;

			$('#tm-saat-ini').val(tm_saat_ini);
		})

		$('#ttr-saat-ini').on('click',function(){
			let tbm_lalu_to_ttr=parseInt($('#tbm-lalu-to-ttr').val()) || 0;
			let tm_ini_to_ttr=parseInt($('#tm-ini-to-ttr').val()) || 0;
			let ttr_lalu=parseInt($('#ttr-lalu').val()) || 0;
			let ttr_ini_to_tbm =parseInt($('#ttr-ini-to-tbm').val()) || 0;
			

			let ttr_saat_ini = tbm_lalu_to_ttr + tm_ini_to_ttr + ttr_lalu - ttr_ini_to_tbm; 

			$('#ttr-saat-ini').val(ttr_saat_ini);
		})

		$('#jumlah').on('click',function(){
			let tbm_saat_ini=parseInt($('#tbm-saat-ini').val()) || 0;
			let tm_saat_ini=parseInt($('#tm-saat-ini').val()) || 0;
			let ttr_saat_ini=parseInt($('#ttr-saat-ini').val()) || 0;
			

			let jumlah = tbm_saat_ini + tm_saat_ini + ttr_saat_ini; 

			$('#jumlah').val(jumlah);
		})

		$('#ton').keyup(function(event) {
			let ton = parseInt($(this).val()) || 0;
			let tm_saat_ini=parseInt($('#tm-saat-ini').val()) || 0;

			let rata_rata = ton / tm_saat_ini * 1000;

			$('#rata-rata').val(parseInt(rata_rata));
		});

		$('#form-add-generate').submit(function(e) {
			e.preventDefault();
			e.stopImmediatePropagation();
			submitTransaksi()
		});

		function submitTransaksi() {
			const button = $('.btn-simpan');
			let Form = $('#form-add-generate').serialize()+ "&csrf_hash_name=" + $('meta[name="csrf-token"]').attr("content");
			console.log(Form);
			$.ajax({
				url: base_url + "dns_perkebunan/transaksi/save",
				type: 'POST',
				data: Form,
				beforeSend: function() {
					button.text("Menyimpan..."); 
					button.attr("disabled", true); 
				},
				success: function(response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					if (response.success) {
						Swal.fire({
							position: 'top-end',
							icon: 'success',
							title: 'Data Berhasil Disimpan',
							showConfirmButton: false,
							timer: 1500
						})
						$('#add-modal').modal('hide');
						window.location.href=base_url+"dns_perkebunan/transaksi/data/"+response.data;

					}else{
						Swal.fire({
							position: 'center',
							icon: 'success',
							title: response.message,
							showConfirmButton: true,
							// timer: 1500
						})
					}
				},
				complete: function() {
					button.text("Simpan"); 
					button.attr("disabled", false); 
				},
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				}
			});
		}

		function updateKepemilikan() {
			const button = $('.btn-simpan');
			let Form = $('#form-edit-kepemilikan').serialize()+ "&csrf_hash_name=" + $('meta[name="csrf-token"]').attr("content");
			$.ajax({
				url: base_url + "dns_perkebunan/kepemilikan/save",
				type: 'POST',
				data: Form,
				beforeSend: function() {
					button.text("Menyimpan..."); 
					button.attr("disabled", true); 
				},
				success: function(response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					if (response.success) {
						Swal.fire({
							position: 'top-end',
							icon: 'success',
							title: 'Data Berhasil Diubah',
							showConfirmButton: false,
							timer: 1500
						})
						$('#edit-modal').modal('hide');
						Datatables();

					}
				},
				complete: function() {
					button.text("Simpan"); 
					button.attr("disabled", false); 
				},
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				}
			});
		}

		function deleteTransaksi(sid){
			$.ajax({
				url: base_url+'dns_perkebunan/transaksi/delete',
				type: 'post',
				data: {
					sid:sid,
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				success: function (response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					Swal.fire({
						position: 'top-end',
						icon: 'success',
						title: 'Data Berhasil Dihapus',
						showConfirmButton: false,
						timer: 1500
					})
					Datatables();
				}
			});
		}

		function dataKecamatan(){
			let opsi_kecamatan = '';
			opsi_kecamatan +='<option value="">Pilih Kecamatan</option>'
			$.ajax({
				url: base_url+'master/data-kecamatan',
				type: 'GET',
				data: {
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				dataType:"json",
				success: function (response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					for(key in response.data){
						let id_ = response.data[key].id_kecamatan
						let name = response.data[key].nama_kecamatan
						opsi_kecamatan +='<option value="'+id_+'">'+name+'</option>'
					}

					$('#id_kecamatan,#e-id_kecamatan').html(opsi_kecamatan);
				}
			});
		}

		function dataKomoditi(){
			let opsi_komoditi = '';
			opsi_komoditi +='<option value="">Pilih komoditi</option>'
			$.ajax({
				url: base_url+'dns_perkebunan/komoditi/databyid',
				type: 'GET',
				data: {
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				dataType:"json",
				success: function (response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					for(key in response.data){
						let id_ = response.data[key].id_komoditi
						let name = response.data[key].nama_komoditi
						opsi_komoditi +='<option value="'+id_+'">'+name+'</option>'
					}

					$('#id_komoditi,#e-id_komoditi').html(opsi_komoditi);
				}
			});
		}

		function dataKepemilikan(){
			let opsi_kepemilikan = '';
			opsi_kepemilikan +='<option value="">Pilih kepemilikan</option>'
			$.ajax({
				url: base_url+'dns_perkebunan/kepemilikan/databyid',
				type: 'GET',
				data: {
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				dataType:"json",
				success: function (response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					for(key in response.data){
						let id_ = response.data[key].id_kepemilikan
						let name = response.data[key].kepemilikan+'-'+response.data[key].keterangan
						opsi_kepemilikan +='<option value="'+id_+'">'+name+'</option>'
					}

					$('#id_kepemilikan,#e-id_kepemilikan').html(opsi_kepemilikan);
				}
			});
		}

		function dataPeriode(){
			let opsi_periode = '';
			opsi_periode +='<option value="">Pilih periode</option>'
			$.ajax({
				url: base_url+'dns_perkebunan/periode/databyid',
				type: 'GET',
				data: {
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				dataType:"json",
				success: function (response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					for(key in response.data){
						let id_ = response.data[key].id_periode
						let name = response.data[key].tahun+' : '+response.data[key].semester
						opsi_periode +='<option value="'+id_+'">'+name+'</option>'
					}

					$('#id_periode,#e-id_periode').html(opsi_periode);
				}
			});
		}

		function searchDataSmstlalu(){
			$.ajax({
				url: base_url+'dns_perkebunan/transaksi/data-smlalu',
				type: 'get',
				data: datas,
				success: function (response) {
					console.log(response);
					if(stats == 'add'){
						if(response.success){
							$('#tbm-lalu').val(response.data['tbm']);
							$('#tm-lalu').val(response.data['tm']);
							$('#ttr-lalu').val(response.data['ttr']);

						}else{
							$('#form-add-transaksi input, select').removeAttr('disabled');
						}
						
					}else{
						$('#form-edit-transaksi input, select').removeAttr('disabled');
					}
					
				},
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				}
			});
		}


	})
</script>


@endsection