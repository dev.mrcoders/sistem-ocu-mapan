@extends('template.index')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">

				<div class="card-body ">
					<div class="row mb-4">
						<div class="col-sm-12">
							<h4 class="card-title">Rekap Data</h4>
						</div>
						<div class="col-sm-11 ">
							<div class="row">
								<div class="col-sm-3">
									<div class="form-group">
										<label for="">Kecamatan</label>
										<select name="" id="id_kecamatan" class="form-control"></select>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<label for="">Periode</label>
										<select name="" id="id_periode" class="form-control"></select>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<label for="">Kepemilikan</label>
										<select name="" id="id_kepemilikan" class="form-control"></select>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<label for="">Komoditi</label>
										<select name="" id="id_komoditi" class="form-control"></select>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-1 text-center ">
							<div class="btn-group btn-group-sm btn-block m-t-30" role="group" aria-label="Basic example">
								<button type="button" class="btn btn-success cari">Cari</button>
								<button type="button" class="btn btn-success reset">Reset</button>
							</div>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table nowrap" id="tb-data" width="100%">
							<thead class="bg-info text-white">
								<tr>
									<th>No</th>
									<th>Kecamatan</th>
									<th>Periode</th>
									<th>Kepemilikan</th>
									<th>Komoditi</th>
									<th>Total Luas Lahan</th>
									<th>Total Produksi</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									
								</tr>
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(function(){
		ajaxcsrf();
		Datatables();
		$("select").select2({
			width: '100%',
			theme: 'bootstrap4',
			allowClear: true,
		});
		function Datatables() {
			$('#tb-data').DataTable({
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
				},
				dom: 'Bfrtip',
				"bFilter": false,
				stateSave: true,
				destroy: true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					"url": base_url + "dns_perkebunan/rekap/data-tables",
					"type": "POST",
					"data": {
						csrf_hash_name: $('meta[name="csrf-token"]').attr("content"),
					},
					"data": function(data) {
						data.csrf_hash_name = $('meta[name="csrf-token"]').attr("content");
						data.kecamatan = $('#id_kecamatan').val();
						data.periode = $('#id_periode').val();
						data.kepemilikan = $('#id_kepemilikan').val();
						data.komoditi = $('#id_komoditi').val();
					},
					"dataSrc": function(response) {
						$('meta[name="csrf-token"]').attr("content", response.csrf_param);
						return response.data;
					},

				},
				"columns": [{
					data:"no"
				},
				{
					data: 'kecamatan',
				},
				{
					data: {
						semester:"semester",
						tahun:"tahun",
					},render:function(d){
						return d.tahun+'-'+d.semester
					}
				},
				{
					data: {
						kepemilikan:"kepemilikan",
						keterangan:"keterangan"
					},render:function(d){
						return d.kepemilikan+' ('+d.keterangan+')'
					}
				},
				{
					data: 'komoditi',
				},
				{
					data: 'luas',
				},
				{

					data: 'total',
				},
				],
				buttons: [
				{
					extend: 'pdfHtml5',
					orientation: 'potrait',
					pageSize: 'A4',
					download: 'open'
				},
				{
					extend: 'print',
				}

				],
				'columnDefs':[{
					'targets':[5,6],
					'orderable':false
				}]
				
				
			});
		}

		$('.cari').on('click',function(event) {
			event.preventDefault();
			Datatables();
		});

		$('.reset').on('click',function(event) {
			event.preventDefault();
			$('#id_kecamatan,#id_periode,#id_kepemilikan,#id_komoditi').val('').change()
			Datatables();
		});
		dataKecamatan()
		function dataKecamatan(){
			let opsi_kecamatan = '';
			opsi_kecamatan +='<option value="">Pilih Kecamatan</option>'
			$.ajax({
				url: base_url+'master/data-kecamatan',
				type: 'GET',
				data: {
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				dataType:"json",
				success: function (response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					for(key in response.data){
						let id_ = response.data[key].id_kecamatan
						let name = response.data[key].nama_kecamatan
						opsi_kecamatan +='<option value="'+id_+'">'+name+'</option>'
					}

					$('#id_kecamatan').html(opsi_kecamatan);
				}
			});
		}
		dataKomoditi()
		function dataKomoditi(){
			let opsi_komoditi = '';
			opsi_komoditi +='<option value="">Pilih komoditi</option>'
			$.ajax({
				url: base_url+'dns_perkebunan/komoditi/databyid',
				type: 'GET',
				data: {
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				dataType:"json",
				success: function (response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					for(key in response.data){
						let id_ = response.data[key].id_komoditi
						let name = response.data[key].nama_komoditi
						opsi_komoditi +='<option value="'+id_+'">'+name+'</option>'
					}

					$('#id_komoditi').html(opsi_komoditi);
				}
			});
		}
		dataKepemilikan()
		function dataKepemilikan(){
			let opsi_kepemilikan = '';
			opsi_kepemilikan +='<option value="">Pilih kepemilikan</option>'
			$.ajax({
				url: base_url+'dns_perkebunan/kepemilikan/databyid',
				type: 'GET',
				data: {
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				dataType:"json",
				success: function (response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					for(key in response.data){
						let id_ = response.data[key].id_kepemilikan
						let name = response.data[key].kepemilikan+'-'+response.data[key].keterangan
						opsi_kepemilikan +='<option value="'+id_+'">'+name+'</option>'
					}

					$('#id_kepemilikan').html(opsi_kepemilikan);
				}
			});
		}
		dataPeriode()
		function dataPeriode(){
			let opsi_periode = '';
			opsi_periode +='<option value="">Pilih periode</option>'
			$.ajax({
				url: base_url+'dns_perkebunan/periode/databyid',
				type: 'GET',
				data: {
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				dataType:"json",
				success: function (response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					for(key in response.data){
						let id_ = response.data[key].id_periode
						let name = response.data[key].tahun+' : '+response.data[key].semester
						opsi_periode +='<option value="'+id_+'">'+name+'</option>'
					}

					$('#id_periode').html(opsi_periode);
				}
			});
		}

	})
</script>


@endsection