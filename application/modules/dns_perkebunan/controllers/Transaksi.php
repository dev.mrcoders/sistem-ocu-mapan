<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends MY_Controller {
	private $data=[];
	private $nip;
	var $csrf_name = '';
	var $csrf_hash;
	var $json = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_transaksi_master','master');
		$this->load->model('M_kepemilikan');
		$this->load->model('M_periode');
		$this->load->model('M_komoditi');
		$this->csrf_name = 'csrf_param';
		$this->csrf_hash = $this->security->get_csrf_hash();
	}

	public function index()
	{
		$this->data['title'] = 'Ini Komoditi';
		$this->view('transaksi.index', $this->data);	
	}

	public function ViewData($sid)
	{
		$this->data['title'] = 'Ini Komoditi';
		$this->data['id_trx'] = $sid;
		$this->view('transaksi.view', $this->data);	
	}

	public function DataTables()
	{
		$list = $this->master->get_datatables();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $r) {
			$no++;
			$row = array();

			$row['no']=$no;
			$row['id_trx'] = $r->id_transaksi_master;
			$row['id_kepemilikan'] = $this->GetKepemilikan($r->id_kepemilikan);
			$row['id_periode'] = $this->GetPeriode($r->id_periode);
			$row['id_komoditi'] = $this->GetKomoditi($r->id_komoditi);

			$data[] = $row;
		}
		$this->json = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->master->count_all(),
			"recordsFiltered" => $this->master->count_filtered(),
			"data" => $data,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));

	}

	public function GetPeriode($sid)
	{
		$get_data_periode = $this->M_periode->GetById($sid);

		return ['semester'=>$get_data_periode->semester,'tahun'=>$get_data_periode->tahun];
	}

	public function GetKepemilikan($sid)
	{
		$get_data_kepemilikan = $this->M_kepemilikan->GetById($sid);

		return $get_data_kepemilikan->kepemilikan;
	}

	public function GetKomoditi($sid)
	{
		$get_data_komoditi = $this->M_komoditi->GetById($sid);

		return $get_data_komoditi->nama_komoditi;
	}

	public function GetDataTrxTanaman()
	{
		$this->json = array(
			"success" => false,
			"message" => "OK",
			"data" => null,
		);
		$get =$this->input->get();
		$get_data_periode = $this->M_periode->GetById($this->input->get('id_periode'));
		$semester = ($get_data_periode->semester == 1 ? 2:1);
		$year = ($get_data_periode->semester == 1? $get_data_periode->tahun - 1: $get_data_periode->tahun);
		$get_periode_year = $this->M_periode->GetByYear(['tahun'=>$year,'semester'=>$semester]);

		$data = [
			'id_kecamatan'=>$get['id_kecamatan'],
			'id_periode'=>$get_periode_year->id_periode,
			'id_kepemilikan'=>$get['id_kepemilikan'],
			'id_komoditi'=>$get['id_komoditi'],
		];
		$get_tanam =$this->master->GetDataTanam($data);


		if($get_tanam){
			$this->json = array(
				"success" => true,
				"message" => "OK",
				"data" => $get_tanam,
			);
		}

		
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	function DataMaster(){
		$data = $this->master->DataTrx('tb_transaksi_master',['id_transaksi_master'=>$this->input->get('sids')]);

		$periode['id_']=$data->id_periode;
		$periode['semester']=$this->GetPeriode($data->id_periode)['semester'];
		$periode['tahun']=$this->GetPeriode($data->id_periode)['tahun'];
		$data_master=[
			'komoditi'=>
			[
				'id_'=>$data->id_komoditi,
				'nama'=>$this->GetKomoditi($data->id_komoditi)
			],
			'kepemilikan'=>['id_'=>$data->id_kepemilikan,'nama'=>$this->GetKepemilikan($data->id_kepemilikan)],
			'periode'=>$periode,
		];
		$this->json = array(
			"success" => true,
			"message" => "OK",
			"data" => $data_master,
		);
		

		
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function GenerateData()
	{
		$post =$this->input->post();

		$is_ada = $this->master->DataTrx('tb_transaksi_master',['id_kepemilikan'=>$post['id_kepemilikan'],'id_komoditi'=>$post['id_komoditi'],'id_periode'=>$post['id_periode']]);

		if($is_ada){
			$this->json = array(
				"success" => false,
				"message" => "Data Sudah Ada Data Tidak bisa disimpan dan digenerate",
				"data" => '',
			);
		}else{
			$transaksi_master = [
				'id_periode'=>$post['id_periode'],
				'id_kepemilikan'=>$post['id_kepemilikan'],
				'id_komoditi'=>$post['id_komoditi'],
				'created_at'=>date('Y-m-d H:i:s'),
				'user_created'=>'admin'
			];
			$id_trx = $this->master->Saved($transaksi_master);

			$this->json = array(
				"success" => true,
				"message" => "OK",
				"data" => $id_trx,
			);
		}

		

		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function Create()
	{
		$post =$this->input->post();

		$transaksi_tanaman =[
			'id_transaksi_master'=>$post['id_trx'],
			'id_kecamatan'=>$post['id_kecamatan'],
			'id_periode'=>$post['id_periode'],
			'id_kepemilikan'=>$post['id_kepemilikan'],
			'id_komoditi'=>$post['id_komoditi'],
			'tbm'=>$post['tbm_saat_ini'],
			'tm'=>$post['tm_saat_ini'],
			'ttr'=>$post['ttr_saat_ini']
		];
		
		$transaksi_produksi =[
			'id_transaksi_master'=>$post['id_trx'],
			'id_kecamatan'=>$post['id_kecamatan'],
			'id_periode'=>$post['id_periode'],
			'id_kepemilikan'=>$post['id_kepemilikan'],
			'id_komoditi'=>$post['id_komoditi'],
			'produksi'=>$post['ton'],
			'rata_rata'=>$post['rata_rata'],
		];
		
		$transaksi_petani =[
			'id_transaksi_master'=>$post['id_trx'],
			'id_kecamatan'=>$post['id_kecamatan'],
			'id_periode'=>$post['id_periode'],
			'id_kepemilikan'=>$post['id_kepemilikan'],
			'id_komoditi'=>$post['id_komoditi'],
			'jumlah_kk'=>$post['petani'],
		];
		
		$transaksi_lahan =[
			'id_transaksi_master'=>$post['id_trx'],
			'id_kecamatan'=>$post['id_kecamatan'],
			'id_periode'=>$post['id_periode'],
			'id_kepemilikan'=>$post['id_kepemilikan'],
			'id_komoditi'=>$post['id_komoditi'],
			'luas_lahan'=>$post['lahan_baru'],
			'total'=>$post['jumlah'],
		];

		$transaksi_edit =[
			'id_transaksi_master'=>$post['id_trx'],
			'id_kecamatan'=>$post['id_kecamatan'],
			'id_periode'=>$post['id_periode'],
			'id_kepemilikan'=>$post['id_kepemilikan'],
			'id_komoditi'=>$post['id_komoditi'],
			'tbm_lalu'=>$post['tbm_lalu'],
			'tbm_ini_to_tm'=>$post['tbm_ini_to_tm'],
			'tbm_ini_to_ttr'=>$post['tbm_ini_to_ttr'],
			'tm_lalu'=>$post['tm_lalu'],
			'tm_ini_to_ttr'=>$post['tm_ini_to_ttr'],
			'ttr_lalu'=>$post['ttr_lalu'],
			'ttr_ini_to_tbm'=>$post['ttr_ini_to_tbm'],
		];

		$transaksi_rekap=[
			'id_kecamatan'=>$post['id_kecamatan'],
			'id_periode'=>$post['id_periode'],
			'id_kepemilikan'=>$post['id_kepemilikan'],
			'id_komoditi'=>$post['id_komoditi'],
			'total_luas_lahan'=>$post['jumlah'],
			'total_produksi'=>$post['ton'],
		];
		
		if($this->input->post('mode') == 'add'){
			$this->master->StoreTrx('tb_transaksi_tanaman',$transaksi_tanaman);
			$this->master->StoreTrx('tb_transaksi_produksi',$transaksi_produksi);
			$this->master->StoreTrx('tb_transaksi_petani',$transaksi_petani);
			$this->master->StoreTrx('tb_transaksi_lahan',$transaksi_lahan);
			$this->master->StoreTrx('tb_transaksi_edit',$transaksi_edit);
			$this->master->StoreTrx('tb_transaksi_rekap',$transaksi_rekap);
		}else{
			$this->master->UpdateTrx('tb_transaksi_tanaman',$transaksi_tanaman);
			$this->master->UpdateTrx('tb_transaksi_produksi',$transaksi_produksi);
			$this->master->UpdateTrx('tb_transaksi_petani',$transaksi_petani);
			$this->master->UpdateTrx('tb_transaksi_lahan',$transaksi_lahan);
			$this->master->UpdateTrx('tb_transaksi_edit',$transaksi_edit);
			$this->master->UpdateTrx('tb_transaksi_rekap',$transaksi_rekap);
		}

		$this->json = array(
			"success" => true,
			"message" => "OK",
			"data" => $post,
		);

		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function Delete()
	{
		$sid = $this->input->post('sid');

		$this->master->Deleted($sid);
		$this->json = array(
			"success" => true,
			"message" => 'ok',
			'data'=>$sid
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function DataTablesTrxData()
	{
		$this->db->where('kd_kecamatan !=', 0);
		$list = $this->db->get('tb_kecamatan')->result();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $r) {
			$no++;
			$row = array();

			$row['no']=$no;
			$row['id_kecamatan'] = $r->id_kecamatan;
			$row['nama_kecamatan'] = $r->nama_kecamatan;
			$row['id_trx'] = $this->input->post('idtrx');
			$row['data_tanaman_lalu'] = $this->DataTanamanLalu($this->input->post('idtrx'),$r->id_kecamatan);
			$row['data_lahan'] = $this->DataLahan($this->input->post('idtrx'),$r->id_kecamatan);
			$row['data_tanaman_ini'] = $this->DataTanamanIni($this->input->post('idtrx'),$r->id_kecamatan);
			$row['data_produksi'] = $this->DataProduksi($this->input->post('idtrx'),$r->id_kecamatan);
			$row['data_petani'] = $this->DataPetani($this->input->post('idtrx'),$r->id_kecamatan);

			$data[] = $row;
		}
		$this->json = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => count($list),
			"recordsFiltered" => count($list),
			"data" => $data,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function DataTanamanLalu($id,$id_kec)
	{
		$get_trx_tanaman = $this->master->DataTrx('tb_transaksi_edit',['id_transaksi_master'=>$id,'id_kecamatan'=>$id_kec]);

		if ($get_trx_tanaman) {
			$this->json = array(
				"tbm_lalu" => $get_trx_tanaman->tbm_lalu,
				"tbm_lalu_to_tm" => $get_trx_tanaman->tbm_ini_to_tm,
				"tbm_lalu_to_ttr" => $get_trx_tanaman->tbm_ini_to_ttr,
				"tm_lalu" => $get_trx_tanaman->tm_lalu,
				"tm_lalu_to_ttr" => $get_trx_tanaman->tm_ini_to_ttr,
				"ttr_lalu" => $get_trx_tanaman->ttr_lalu,
				"ttr_lalu_to_tbm" => $get_trx_tanaman->ttr_ini_to_tbm,
			);
		}else{
			$this->json = array(
				"tbm_lalu" => 0,
				"tbm_lalu_to_tm" => 0,
				"tbm_lalu_to_ttr" => 0,
				"tm_lalu" => 0,
				"tm_lalu_to_ttr" => 0,
				"ttr_lalu" => 0,
				"ttr_lalu_to_tbm" => 0,
			);
		}

		return $this->json;
	}

	public function DataLahan($id,$id_kec)
	{
		$get_trx_tanaman = $this->master->DataTrx('tb_transaksi_lahan',['id_transaksi_master'=>$id,'id_kecamatan'=>$id_kec]);

		if ($get_trx_tanaman) {
			$this->json = array(
				"luas" => $get_trx_tanaman->luas_lahan,
				"total" => $get_trx_tanaman->total,
			);
		}else{
			$this->json = array(
				"luas" => 0,
				"total" => 0,
			);
		}

		return $this->json;
	}

	public function DataTanamanIni($id,$id_kec)
	{
		$get_trx_tanaman = $this->master->DataTrx('tb_transaksi_tanaman',['id_transaksi_master'=>$id,'id_kecamatan'=>$id_kec]);

		if ($get_trx_tanaman) {
			$this->json = array(
				"tbm" => $get_trx_tanaman->tbm,
				"tm" => $get_trx_tanaman->tm,
				"ttr" => $get_trx_tanaman->ttr,
			);
		}else{
			$this->json = array(
				"tbm" => 0,
				"tm" => 0,
				"ttr" => 0,
			);
		}

		return $this->json;
	}

	public function DataProduksi($id,$id_kec)
	{
		$get_trx_tanaman = $this->master->DataTrx('tb_transaksi_produksi',['id_transaksi_master'=>$id,'id_kecamatan'=>$id_kec]);

		if ($get_trx_tanaman) {
			$this->json = array(
				"produksi" => $get_trx_tanaman->produksi,
				"rata_rata" => $get_trx_tanaman->rata_rata,
			);
		}else{
			$this->json = array(
				"produksi" => 0,
				"rata_rata" => 0,
			);
		}

		return $this->json;
	}

	public function DataPetani($id,$id_kec)
	{
		$get_trx_tanaman = $this->master->DataTrx('tb_transaksi_petani',['id_transaksi_master'=>$id,'id_kecamatan'=>$id_kec]);

		if ($get_trx_tanaman) {
			$this->json = array(
				"petani" => $get_trx_tanaman->jumlah_kk,
			);
		}else{
			$this->json = array(
				"petani" => 0,
			);
		}

		return $this->json;
	}

	public function GetTransaksiEdit()
	{
		$get = $this->input->get();
		unset($get['csrf_hash_name']);

		$get_trx_edit = $this->master->DataTrx('tb_transaksi_edit',$get);
		$get_trx_lahan = $this->master->DataTrx('tb_transaksi_lahan',$get);
		$get_trx_petani = $this->master->DataTrx('tb_transaksi_petani',$get);
		$get_trx_produksi = $this->master->DataTrx('tb_transaksi_produksi',$get);
		$get_trx_tanaman = $this->master->DataTrx('tb_transaksi_tanaman',$get);

		$this->json = array(
			"success" => true,
			"message" => "OK",
			"data" => [
				'edit'=>$get_trx_edit,
				'lahan'=>$get_trx_lahan,
				'petani'=>$get_trx_petani,
				'produksi'=>$get_trx_produksi,
				'tanaman'=>$get_trx_tanaman,
			],
		);

		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

}

/* End of file Transaksi.php */
/* Location: ./application/modules/dns_perkebunan/controllers/Transaksi.php */