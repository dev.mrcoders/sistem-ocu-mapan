<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap extends MY_Controller {
	private $data=[];
	private $nip;
	var $csrf_name = '';
	var $csrf_hash;
	var $json = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_rekap');
		$this->load->model('master/M_kecamatan','kecamatan');
		$this->csrf_name = 'csrf_param';
		$this->csrf_hash = $this->security->get_csrf_hash();
	}

	public function index()
	{
		$this->data['title'] = 'Data Rekap';
		$this->view('rekap.index', $this->data);	
	}

	public function DataTables()
	{
		$list = $this->M_rekap->get_datatables();
		$data = array();
		$no = $this->input->post('start');
			//looping data mahasiswa
		foreach ($list as $r) {
			$no++;
			$row = array();

			$row['no']=$no;
			$row['kecamatan'] = $this->dataKecamatan($r->id_kecamatan);
			$row['komoditi'] = $r->nama_komoditi;
			$row['kepemilikan'] = $r->kepemilikan;
			$row['keterangan'] = $r->keterangan;
			$row['semester'] = $r->semester;
			$row['tahun'] = $r->tahun;
			$row['luas'] = $r->total_luas_lahan;
			$row['total'] = $r->total_produksi;

			$data[] = $row;
		}
		$this->json = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->M_rekap->count_all(),
			"recordsFiltered" => $this->M_rekap->count_filtered(),
			"data" => $data,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));

	}


	public function dataKecamatan($sid)
	{
		$get_kecamatan = $this->kecamatan->GetKecamatanById($sid);

		return $get_kecamatan->nama_kecamatan;
	}

}

/* End of file Samples.php */
	/* Location: ./application/modules/Dns_perikanan/controllers/Samples.php */