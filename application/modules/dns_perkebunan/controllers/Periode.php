<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periode extends MY_Controller {
	private $data=[];
	private $nip;
	var $csrf_name = '';
	var $csrf_hash;
	var $json = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_periode');
		$this->csrf_name = 'csrf_param';
		$this->csrf_hash = $this->security->get_csrf_hash();
	}

	public function index()
	{
		$this->data['title'] = 'Ini Komoditi';
		$this->view('periode.index', $this->data);	
	}

	public function DataTables()
	{
		$list = $this->M_periode->get_datatables();
		$data = array();
		$no = $this->input->post('start');
			//looping data mahasiswa
		foreach ($list as $r) {
			$no++;
			$row = array();

			$row['no']=$no;
			$row['id_periode'] = $r->id_periode;
			$row['semester'] = $r->semester;
			$row['tahun'] = $r->tahun;

			$data[] = $row;
		}
		$this->json = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->M_periode->count_all(),
			"recordsFiltered" => $this->M_periode->count_filtered(),
			"data" => $data,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));

	}

	public function GetDataById()
	{

		$get_data_komoditi = $this->M_periode->GetById();

		$this->json = array(
			"success" => true,
			"message" => 'ok',
			'data'=>$get_data_komoditi
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function Create()
	{
		$post=$this->input->post();

		if($this->input->post('id_periode')){
			$this->M_periode->Updated($post);
		}else{
			$this->M_periode->Saved($post);
		}


		$this->json = array(
			"success" => true,
			"message" => 'ok',
			'data'=>$post
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function Delete()
	{
		$sid = $this->input->post('sid');

		$this->M_periode->Deleted($sid);
		$this->json = array(
			"success" => true,
			"message" => 'ok',
			'data'=>$sid
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}


}

/* End of file Samples.php */
	/* Location: ./application/modules/Dns_perikanan/controllers/Samples.php */