<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_transaksi_master extends CI_Model {
	var $disbun_db;
	var $table = 'tb_transaksi_master';
	var $primary_key= 'id_transaksi_master';
	var $column_order = array(null,'id_periode','id_kepemilikan','id_komoditi','created_at');
	var $column_search = array('id_periode','id_kepemilikan','id_komoditi');
	var $order = array('id_transaksi_master' => 'asc');

	public function __construct()
	{
		parent::__construct();
		$this->disbun_db = $this->load->database('disbun',TRUE);
	}

	private function _get_datatables_query()
	{
		$this->disbun_db->from($this->table);

		$i = 0;
		foreach ($this->column_search as $item) 
		{
			if ($this->input->post('search')['value']) 
			{
				if ($i === 0) 
				{
					$this->disbun_db->group_start();
					$this->disbun_db->like($item, $this->input->post('search')['value']);
				}else{
					$this->disbun_db->or_like($item, $this->input->post('search')['value']);
				}
				if (count($this->column_search) - 1 == $i) 
					$this->disbun_db->group_end();
			}
			$i++;
		}

		if ($this->input->post('order')) {
			$this->disbun_db->order_by($this->column_order[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->disbun_db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if ($this->input->post('length') != -1)
			$this->disbun_db->limit($this->input->post('length'), $this->input->post('start'));
		$query = $this->disbun_db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->disbun_db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->disbun_db->from($this->table);
		return $this->disbun_db->count_all_results();
	}

	public function Saved($data)
	{
		$this->disbun_db->insert($this->table,$data);
		return $this->disbun_db->insert_id();
	}

	public function Updated($data)
	{
		$this->disbun_db->where($this->primary_key,$data['id_periode']);
		return $this->disbun_db->update($this->table,['semester'=>$data['semester'],'tahun'=>$data['tahun']]);
	}

	public function GetById($id)
	{
		return $this->disbun_db->get_where($this->table,[$this->primary_key=>$id])->row();
	}

	public function Deleted($id)
	{
		$this->disbun_db->where($this->primary_key,$id);
		$this->disbun_db->delete($this->table);

		$this->disbun_db->where($this->primary_key,$id);
		$this->disbun_db->delete('tb_transaksi_lahan');

		$this->disbun_db->where($this->primary_key,$id);
		$this->disbun_db->delete('tb_transaksi_petani');

		$this->disbun_db->where($this->primary_key,$id);
		$this->disbun_db->delete('tb_transaksi_produksi');

		$this->disbun_db->where($this->primary_key,$id);
		return $this->disbun_db->delete('tb_transaksi_tanaman');
	}

	public function GetDataTanam($data)
	{
		unset($data['csrf_hash_name']);
		return $this->disbun_db->get_where('tb_transaksi_tanaman',$data)->row();
	}

	public function StoreTrx($table_trx,$data)
	{
		return $this->disbun_db->insert($table_trx,$data);
	}

	public function UpdateTrx($table_trx,$data)
	{
		if($table_trx != 'tb_transaksi_rekap'){
			$this->disbun_db->where('id_transaksi_master', $data['id_transaksi_master']);
		}
		
		$this->disbun_db->where('id_kecamatan', $data['id_kecamatan']);
		$this->disbun_db->where('id_komoditi', $data['id_komoditi']);
		$this->disbun_db->where('id_periode', $data['id_periode']);
		return $this->disbun_db->update($table_trx,$data);
	}

	public function DataTrx($table_trx,$data)
	{
		return $this->disbun_db->get_where($table_trx,$data)->row();
	}

}

/* End of file M_komoditi.php */
	/* Location: ./application/modules/dns_perkebunan/models/M_komoditi.php */