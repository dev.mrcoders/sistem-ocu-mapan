<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {
	private $data=[];
	var $csrf_name = '';
	var $csrf_hash;
	var $json = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_auth');
		$this->csrf_name = 'csrf_param';
		$this->csrf_hash = $this->security->get_csrf_hash();
	}

	public function index()
	{
		if($this->session->userdata('login') != FALSE){
			redirect(base_url().'home','refresh');
		}else{
			$this->data['title'] = 'Ini Home';
			$this->view('form_login', $this->data);
		}
		
	}

	public function CheckingAuth()
	{
		$Post=[
			'username'=>$this->input->post('username'),
			'password'=>md5($this->config->item('salt_md5').$this->input->post('password').$this->config->item('salt_md5'))
		];

		if($this->input->post('chaptcha') == $this->session->userdata('kode')){
			$checking_db = $this->M_auth->AuthCheckingUser($Post);
			if($checking_db){
				$userdata = array(
					'username' => $checking_db->nama,
					'nip' => $checking_db->nip,
					'login' => TRUE
				);
				
				$this->session->set_userdata( $userdata );
				$modules = $this->access->getmodules($userdata['nip']);
				$this->json=[
					'status'=>true,
					'message'=>"Username Dan Password OK",
					'data'=>base_url('').$modules->nama_modules.'/home'
				];
			}else{
				$this->json=[
					'status'=>false,
					'message'=>"Username atau Password Salah",
					'data'=>$Post
				];
			}

		}else{
			$this->json=[
				'status'=>false,
				'message'=>"Chapctha Salah",
				'data'=>$Post
			];
			
		}

		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));

		
	}

	public function LogOut()
	{
		$userdata = array(
			'username' => '',
			'nip' => '',
			'login' => FALSE
		);
		$this->session->unset_userdata($userdata); //clear session
		$this->session->sess_destroy(); //tutup session
		redirect(base_url());
	}


}

/* End of file Samples.php */
	/* Location: ./application/modules/Dns_perikanan/controllers/Samples.php */