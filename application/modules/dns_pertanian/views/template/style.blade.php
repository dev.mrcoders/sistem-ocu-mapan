<!-- Custom CSS -->
<link href="@asset('/')assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
<link href="@asset('/')assets/extra-libs/c3/c3.min.css" rel="stylesheet">
<link href="@asset('/')assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
<!-- Custom CSS -->
<link href="@asset('/')dist/css/style.min.css" rel="stylesheet">
{{-- css datatables --}}
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.4.0/css/responsive.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.3.1/css/rowReorder.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/rowgroup/1.3.0/css/rowGroup.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.3.6/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="@asset('node_modules/')@sweetalert2/theme-bulma/bulma.css">
<link rel="stylesheet" type="text/css" href="@asset('/')assets/libs/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="@asset('/')assets/libs/jquery/dist/jquery.min.js"></script>

<style>
	.w-15 {
		width: 15%!important;
	}
</style>