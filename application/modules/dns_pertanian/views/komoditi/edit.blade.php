<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Ubah Komoditi</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="form-edit-komoditi">
				<div class="modal-body">
					<div class="form-group">
						<label for="recipient-name" class="control-label">Nama Komoditi:</label>
						<input type="hidden" name="id_komoditi" id="id_komoditi" value="" required>
						<input type="text" class="form-control" id="edit_nama_komoditi" name="nama_komoditi" required>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-danger waves-effect waves-light btn-simpan">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>