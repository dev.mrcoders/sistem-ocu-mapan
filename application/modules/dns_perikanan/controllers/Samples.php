<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Samples extends MY_Controller {
	private $data=[];

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$this->data['title'] = 'Ini Home';
		$this->view('index', $this->data);
	}

	public function tentang(Type $var = null)
	{
		$this->data['title'] = 'Ini Abaout';
		$this->view('about', $this->data);
	}

}

/* End of file Samples.php */
/* Location: ./application/modules/Dns_perikanan/controllers/Samples.php */