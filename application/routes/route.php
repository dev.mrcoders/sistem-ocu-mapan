<?php

/* 
docs off route CodeIgniter Laravel-like static routes and filters
https://github.com/Patroklo/codeigniter-static-laravel-routes

BladeOne Blade Template Engine docs
https://github.com/EFTEC/BladeOne
*/

Route::filter('logged_in', function () {

	if ($this->session->userdata('login') == FALSE) {
		$this->session->set_flashdata('session', 'Session Sudah Habis Silahkan Login');
		redirect(base_url());
	}
});


Route::get('default_controller', 'authentication/login');

Route::get('chaptcha','authentication/captcha/showcaptcha');
Route::get('chaptcha-result','authentication/captcha/resultcaptcha');
Route::post('auth-checking','authentication/login/checkingauth');
Route::get('logout','authentication/login/logout');

Route::get('home','dashboard', array('before' => 'logged_in'));

Route::prefix('master',function(){
	Route::get('data-kecamatan','master/kecamatan/getdatakecamatan', array('before' => 'logged_in'));
});

Route::prefix('dns_perkebunan', function() {
	Route::get('home','dns_perkebunan/dashboard', array('before' => 'logged_in'));

	Route::get('komoditi', 'dns_perkebunan/komoditi/index', array('before' => 'logged_in'));
	Route::prefix('komoditi',function() {
		Route::post('data-tables','dns_perkebunan/komoditi/datatables', array('before' => 'logged_in'));
		Route::post('save','dns_perkebunan/komoditi/create', array('before' => 'logged_in'));
		Route::get('databyid','dns_perkebunan/komoditi/getdatabyId', array('before' => 'logged_in'));
		Route::post('delete','dns_perkebunan/komoditi/delete', array('before' => 'logged_in'));
	});

	Route::get('periode', 'dns_perkebunan/periode/index', array('before' => 'logged_in'));
	Route::prefix('periode',function() {
		Route::match(['POST','GET'],'data-tables','dns_perkebunan/periode/datatables', array('before' => 'logged_in'));
		Route::post('save','dns_perkebunan/periode/create', array('before' => 'logged_in'));
		Route::get('databyid','dns_perkebunan/periode/getdatabyId', array('before' => 'logged_in'));
		Route::post('delete','dns_perkebunan/periode/delete', array('before' => 'logged_in'));
	});

	Route::get('kepemilikan', 'dns_perkebunan/kepemilikan/index', array('before' => 'logged_in'));
	Route::prefix('kepemilikan',function() {
		Route::match(['POST','GET'],'data-tables','dns_perkebunan/kepemilikan/datatables', array('before' => 'logged_in'));
		Route::post('save','dns_perkebunan/kepemilikan/create', array('before' => 'logged_in'));
		Route::get('databyid','dns_perkebunan/kepemilikan/getdatabyId', array('before' => 'logged_in'));
		Route::post('delete','dns_perkebunan/kepemilikan/delete', array('before' => 'logged_in'));
	});

	Route::get('transaksi', 'dns_perkebunan/transaksi/index', array('before' => 'logged_in'));
	Route::prefix('transaksi',function() {
		Route::match(['POST','GET'],'data-tables','dns_perkebunan/transaksi/datatables', array('before' => 'logged_in'));
		Route::post('save','dns_perkebunan/transaksi/generatedata', array('before' => 'logged_in'));
		Route::post('inserted','dns_perkebunan/transaksi/create', array('before' => 'logged_in'));
		Route::get('databyid','dns_perkebunan/transaksi/datamaster', array('before' => 'logged_in'));
		Route::post('delete','dns_perkebunan/transaksi/delete', array('before' => 'logged_in'));
		Route::get('data-smlalu','dns_perkebunan/transaksi/getdatatrxtanaman', array('before' => 'logged_in'));
		Route::get('data/(:any)','dns_perkebunan/transaksi/viewdata/$1', array('before' => 'logged_in'));
		Route::match(['POST','GET'],'data-trx-view','dns_perkebunan/transaksi/datatablestrxdata', array('before' => 'logged_in'));
		Route::get('data-trx','dns_perkebunan/transaksi/gettransaksiedit', array('before' => 'logged_in'));
	});
	Route::get('rekap', 'dns_perkebunan/rekap/index', array('before' => 'logged_in'));
	Route::prefix('rekap',function() {
		Route::match(['POST','GET'],'data-tables','dns_perkebunan/rekap/datatables', array('before' => 'logged_in'));
		Route::post('save','dns_perkebunan/kepemilikan/create', array('before' => 'logged_in'));
		Route::get('databyid','dns_perkebunan/kepemilikan/getdatabyId', array('before' => 'logged_in'));
		Route::post('delete','dns_perkebunan/kepemilikan/delete', array('before' => 'logged_in'));
	});
});

Route::prefix('dns_perternakan',function(){
	Route::get('home','dns_perternakan/dashboard', array('before' => 'logged_in'));

	Route::get('komoditi', 'dns_perternakan/komoditi/index', array('before' => 'logged_in'));
	Route::prefix('komoditi',function() {
		Route::match(['GET','POST'],'data-tables','dns_perternakan/komoditi/datatables', array('before' => 'logged_in'));
		Route::post('save','dns_perternakan/komoditi/create', array('before' => 'logged_in'));
		Route::get('databyid','dns_perternakan/komoditi/getdatabyId', array('before' => 'logged_in'));
		Route::post('delete','dns_perternakan/komoditi/delete', array('before' => 'logged_in'));
	});

	Route::get('transaksi', 'dns_perternakan/transaksi/index', array('before' => 'logged_in'));
	Route::prefix('transaksi',function() {
		Route::match(['GET','POST'],'data-tables','dns_perternakan/transaksi/datatables', array('before' => 'logged_in'));
		Route::post('save','dns_perternakan/komoditi/create', array('before' => 'logged_in'));
		Route::get('lihat-data/(:any)','dns_perternakan/transaksi/viewdata/$1', array('before' => 'logged_in'));
		Route::get('detail-data','dns_perternakan/transaksi/databyid', array('before' => 'logged_in'));
		Route::get('detail-edit','dns_perternakan/transaksi/detailbyid', array('before' => 'logged_in'));
		Route::match(['GET','POST'],'tables-view','dns_perternakan/transaksi/datatablesview', array('before' => 'logged_in'));
		Route::post('delete','dns_perternakan/komoditi/delete', array('before' => 'logged_in'));
		Route::post('create-data','dns_perternakan/transaksi/saved', array('before' => 'logged_in'));
		Route::post('generate-data','dns_perternakan/transaksi/createdata',array('before' => 'logged_in'));
	});

	Route::get('rekap', 'dns_perternakan/rekap/index', array('before' => 'logged_in'));
	Route::prefix('rekap',function() {
		Route::match(['GET','POST'],'data-tables','dns_perternakan/rekap/datatables', array('before' => 'logged_in'));
		
	});
});


Route::prefix('dns_pertanian',function(){
	Route::get('home','dns_pertanian/dashboard', array('before' => 'logged_in'));

	Route::get('komoditi', 'dns_pertanian/komoditi/index', array('before' => 'logged_in'));
	Route::prefix('komoditi',function() {
		Route::post('data-tables','dns_pertanian/komoditi/datatables', array('before' => 'logged_in'));
		Route::post('save','dns_pertanian/komoditi/create', array('before' => 'logged_in'));
		Route::get('databyid','dns_pertanian/komoditi/getdatabyId', array('before' => 'logged_in'));
		Route::post('delete','dns_pertanian/komoditi/delete', array('before' => 'logged_in'));
	});
});



